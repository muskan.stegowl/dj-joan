//
//  AppDelegate.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 08/03/21.
//

import UIKit
import CoreData
import UIKit
import FirebaseCore
import SVProgressHUD
import Firebase
import AudioToolbox
import IQKeyboardManagerSwift
import FirebaseMessaging
import FirebaseCore
import UserNotifications
import FirebaseInstanceID
import AVKit
import StreamingKit
import CallKit
import MediaPlayer
import GoogleMobileAds
@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate,GADInterstitialDelegate {
    
    var window: UIWindow?
    var audioPlayer = STKAudioPlayer()
    var radioPlayer = STKAudioPlayer()
    var player = AVPlayer()
    var playerItem : AVPlayerItem!
    var playerLayer = AVPlayerLayer()
    var callObserver = CXCallObserver()
    var gcmMessageIDKey = "gcm.message_id"
    let commandCenter = MPRemoteCommandCenter.shared()
    var isFirst = false
    var isHomeVideo = false
    var deviceOrientation = UIInterfaceOrientationMask.portrait
    var isPopupVideo = false
    var google_adsKey = ""
    var google_interstitial = ""
    var interstitial: GADInterstitial!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        configureFirebase(application: application)
        configureSVProgressHUD()
        configureIQKeyboardManager()
        configureReachability()
        WBGetGoogleAdsKey()
       
        
//        audioPlayer.delegate = self
//        radioPlayer.delegate = self
//        callObserver.setDelegate(self, queue: nil)
       
        background_play()
        configurePlayer()
      
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return deviceOrientation
    }
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                FcmID = result.token
                Messaging.messaging().apnsToken = deviceToken as Data
                if Messaging.messaging().apnsToken != nil
                {
                    Messaging.messaging().subscribe(toTopic: "dj_durisimo")
                }
            }
        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
   
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
       
        Messaging.messaging().subscribe(toTopic: "hmg")
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([[.alert, .sound]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
//        Defaults.shared.fcmtokentoken = fcmToken ?? ""
       
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
       
        
       
        // callRegisterUser()
    }
    
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Dj_Kenny")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    func configureSVProgressHUD() {
        
        SVProgressHUD.setDefaultAnimationType(.flat)
        SVProgressHUD.setRingThickness(3)
        SVProgressHUD.setDefaultMaskType(.black)
        
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setBackgroundColor(.clear)
        SVProgressHUD.setForegroundColor(UIColor(named: "MyTealColor")!)
    }
    func configureFirebase(application: UIApplication) {
        
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            UNUserNotificationCenter.current().delegate = self
            UIApplication.shared.applicationIconBadgeNumber = 0
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {granted, error in
                    if let err = error {
                        print("Notification is not granted. Error: \(err.localizedDescription)")
                    }
                    if granted {
                        print("Notification is granted.")
                    }
            })
            
            
            Messaging.messaging().delegate = self as? MessagingDelegate
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
    }
    func updateFirestorePushTokenIfNeeded() {
            if let token = Messaging.messaging().fcmToken {
                print("FCM Token-------",token)
                FcmID = token
//                UserDefaults.standard.token = token
            }
        }
        
    func reloadInterstitial() -> GADInterstitial {
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
        if google_interstitial == ""{
            self.interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        }else{
            self.interstitial = GADInterstitial(adUnitID: google_interstitial)
        }
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
        //        }
    }
    func ShowAds() {
        
        if appdelegate.interstitial.isReady {
            appdelegate.interstitial.present(fromRootViewController:(appdelegate.window?.rootViewController)!)
        } else {
            print("Ad wasn't ready")
        }
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        //        interstitial = reloadInterstitial()
    }
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        ShowAds()
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
        //        self.interstitial = reloadInterstitial()
    }
    
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    func WBGetGoogleAdsKey() {
        
        let url =  API.GoogleAds
        
        print(url)
       
        
        AFWrapper.requestGETURL(url, enableJWT: false, headers: nil, success: { (Response) -> Void in
            print(Response)
            
            let dic = Response as NSDictionary?
            if getInteger(dic?["status"]) == 200{
                
                if let data = Response["data"] as? [[String : Any]] {
                    for item in data{
                        self.google_adsKey = getString(item["ios_banner"])
                        self.google_interstitial = getString(item["ios_interstitial"])
                        
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                            self.interstitial = self.reloadInterstitial()
//                        }
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                            self.ShowAds()
//                        }
                    }
                }
                
            }
     
 }){ (error) -> Void in
     print(error)
     //
     
         }
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        updateFirestorePushTokenIfNeeded()
    }
  


    
    
    func background_play()
    {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
            
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    func configureIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
    }
    func configurePlayer(){
        if isLastPlay == "r"{
            commandCenter.previousTrackCommand.isEnabled = false
            commandCenter.playCommand.isEnabled = true
            commandCenter.pauseCommand.isEnabled = true
            commandCenter.nextTrackCommand.isEnabled = false
            commandCenter.changePlaybackRateCommand.isEnabled = true
        }else{
            commandCenter.previousTrackCommand.isEnabled = true
            commandCenter.playCommand.isEnabled = true
            commandCenter.pauseCommand.isEnabled = true
            commandCenter.nextTrackCommand.isEnabled = true
            commandCenter.changePlaybackRateCommand.isEnabled = true
        }
        
      
        
        
        commandCenter.previousTrackCommand.addTarget { [unowned self] event in
            if  commandCenter.playCommand.isEnabled{
                commandCenter.playCommand.isEnabled = false
                commandCenter.pauseCommand.isEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.Previous()
                }
               
            }
            
           
            return .success
        }
        commandCenter.nextTrackCommand.addTarget { [unowned self] event in
            if  commandCenter.playCommand.isEnabled{
                commandCenter.playCommand.isEnabled = false
                commandCenter.pauseCommand.isEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.Next()
                }
               
            }
            
            return .success
        }
        commandCenter.playCommand.addTarget { [unowned self] event in
            if  commandCenter.playCommand.isEnabled{
                commandCenter.playCommand.isEnabled = false
                commandCenter.pauseCommand.isEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.playPause()
                }
               
            }
            
            return .success
        }
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            if  commandCenter.playCommand.isEnabled{
                commandCenter.playCommand.isEnabled = false
                commandCenter.pauseCommand.isEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.playPause()
                }
               
            }
            
            return .success
        }
       
    }
    
    
    @objc func Next(){
        if isLastPlay == "s"{
            MusicControll().nextSong()
        }else{
            MusicControll().NextVideo()
        }
        
    }
    @objc func Previous(){
        if isLastPlay == "s"{
            MusicControll().previousSong()
            
        }else{
            MusicControll().PreviousVideo()
        }
    }
    
    @objc func playPause(){
        
        if isLastPlay == "s"{
            if playPauseSelected == 0{
                playPauseSelected = 1
                audioPlayer.resume()
            }else{
                playPauseSelected = 0
                audioPlayer.pause()
            }
            
        }else if isLastPlay == "r"{
            
            if isRadio{
                isRadio = false
                radioPlayer.pause()
            }else{
                isRadio = true
                radioPlayer.resume()
            }
        }else if isLastPlay == "v"{
            if isVideoPlay{
                isVideoPlay = false
                player.pause()
            }else{
                isVideoPlay = true
                player.play()
            }
        }
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
    }
    func configureReachability() {
        do {
            try Network.reachability = Reachability(hostname: "www.google.com")
        }
        catch {
            switch error as? Network.Error {
            case let .failedToCreateWith(hostname)?:
                print("Network error:\nFailed to create reachability object With host named:", hostname)
            case let .failedToInitializeWith(address)?:
                print("Network error:\nFailed to initialize reachability object With address:", address)
            case .failedToSetCallout?:
                print("Network error:\nFailed to set callout")
            case .failedToSetDispatchQueue?:
                print("Network error:\nFailed to set DispatchQueue")
            case .none:
                print(error)
            }
        }
    }
}

//MARK:- UIAPPLICATION EXTENSION
extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        //        if let slide = viewController as? SideMenuController{
        //            return topViewController(slide.menuViewController)
        //        }
        return viewController
    }
}
