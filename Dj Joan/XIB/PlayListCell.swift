//
//  PlayListCell.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 04/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit

class PlayListCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
