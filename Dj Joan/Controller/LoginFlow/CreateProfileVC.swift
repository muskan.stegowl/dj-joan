//
//  CreateProfileVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 10/03/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class CreateProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate{
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var imgConPassword: UIImageView!
    @IBOutlet weak var txtConPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnCreateProfile: UIButton!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var imgProfilePic: UIImageView!
    var ConPassHideShow = true
    var passHideShow = true
    var RememberMe = false
    var imageUrl = ""
    var video = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loaddesign()
    }
    func loaddesign(){
        
        if liveVideoMenusStatus == false{
            btnVideo.isHidden = true
            imgVideo.isHidden = true
        }
        
        txtUserName.attributedPlaceholder = NSAttributedString(string: "Enter username", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtConPassword.attributedPlaceholder = NSAttributedString(string: "Enter confirm password", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Enter password", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtPhoneNo.attributedPlaceholder = NSAttributedString(string: "Enter phone number", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter email", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtName.attributedPlaceholder = NSAttributedString(string: "Enter name", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtPhoneNo.delegate = self
        btnCreateProfile.layer.cornerRadius = 15
        btnCreateProfile.clipsToBounds = true
        view1.layer.cornerRadius = 15
        view1.clipsToBounds = true
        view2.layer.cornerRadius = 15
        view2.clipsToBounds = true
        view3.layer.cornerRadius = 15
        view3.clipsToBounds = true
        view4.layer.cornerRadius = 15
        view4.clipsToBounds = true
        view5.layer.cornerRadius = 15
        view5.clipsToBounds = true
        view6.layer.cornerRadius = 15
        view6.clipsToBounds = true
        //        viewRememberMe.layer.cornerRadius = 10
        // imgRememberMe.image = UIImage(named: "")
        //viewRememberMe.clipsToBounds = true
        txtConPassword.isSecureTextEntry = true
        txtPassword.isSecureTextEntry = true
        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.height / 2
        imgProfilePic.clipsToBounds = true
        
        txtUserName.tintColor = UIColor.white
        txtConPassword.tintColor = UIColor.white
        txtPassword.tintColor = UIColor.white
        txtPhoneNo.tintColor = UIColor.white
        txtEmail.tintColor = UIColor.white
        txtName.tintColor = UIColor.white
        //        imgPassHide.tintColor =
        //            UIColor(hex: 0x434343)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = txtPhoneNo.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        txtPhoneNo.text = formattedNumber(number: newString)
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBAction func btnLiveVideo(_ sender: Any) {
        video = true
        CallSkipUserApi()
    }
    @IBAction func btnHome(_ sender: Any) {
        video = false
        CallSkipUserApi()
    }
    @IBAction func btnEditeProfilePic(_ sender: Any) {
        showAlert1()
    }
    @IBAction func btnConPasswordHideShowClick(_ sender: Any) {
        if ConPassHideShow == true{
            ConPassHideShow = false
            txtConPassword.isSecureTextEntry = false
            imgConPassword.image = UIImage(named: "hide_pass")
        }else{
            ConPassHideShow = true
            txtConPassword.isSecureTextEntry = true
            imgConPassword.image = UIImage(named: "show_pass")
        }
    }
    @IBAction func btnPasswordHideShowClick(_ sender: Any) {
        if passHideShow == true{
            passHideShow = false
            txtPassword.isSecureTextEntry = false
            imgPassword.image = UIImage(named: "hide_pass")
        }else{
            passHideShow = true
            txtPassword.isSecureTextEntry = true
            imgPassword.image = UIImage(named: "show_pass")
        }
        
    }
    @available(iOS 13.0, *)
    @IBAction func btnCreateProfileClick(_ sender: Any) {
        if validation(){
            if #available(iOS 13.0, *) {
                CallCreateProfileApi()
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
        GoBack()
    }
    
    private func showAlert1() {
        
        let alert = UIAlertController(title: "Image Selection", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var image : UIImage!
             if let img = info[.editedImage] as? UIImage {
                 image = img
             } else if let img = info[.originalImage] as? UIImage {
                 image = img
             }
           dismiss(animated: true,completion: nil)
        
//        self.dismiss(animated: true) { [weak self] in
            
//            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            //Setting image to your image view
        self.imgProfilePic.image = image
            if #available(iOS 13.0, *) {
                self.uploadImageInDatabase(image:image)
            } else {
                // Fallback on earlier versions
            }
//        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func validation() -> Bool
    {
        if isEmptyString(txtName.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterName)
            
            return false
        }
        else if isEmptyString(txtUserName.text! as NSString)
        {
            
            showMyAlert(myMessage:MessageStrings().pleaseEnterUsername)
            
            return false
        }
        else if isEmptyString(txtEmail.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterEmail)
            
            return false
        }
        else if validateEmailWithString(txtEmail.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterAValidEmail)
            
            return false
        }
        else if isEmptyString(txtPhoneNo.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterPhoneNumber)
            
            return false
        }
        else if (txtPhoneNo.text?.count)! < 10
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterValidPhoneNumber)
            
        }
        else if isEmptyString(txtPassword.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterPassword)
            
            return false
        }
        else if isEmptyString(txtConPassword.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterConfirmPassword)
            
            return false
        }
        else if txtPassword.text != txtConPassword.text{
            
            showMyAlert(myMessage:MessageStrings().passwordAndConfirmPasswordNotMatch)
            
            return false
        }
        return true
    }
}
@available(iOS 13.0, *)
extension CreateProfileVC{
    func CallCreateProfileApi() {
        
        
        let PhoneNo = txtPhoneNo.text!.replacingOccurrences(of: "-", with: "")
        let url = API.register
        //
        let parameter = [
            
            "fcm_id" :FcmID,
            "name" : txtName.text!,
            "username" : txtUserName.text!,
            "email" : txtEmail.text!,
            "phone" : txtPhoneNo.text!,
            "password" : txtPassword.text!,
            "confirm_password" : txtConPassword.text!,
            //                          "image":imageUrl,
            "device" : "iOS"
            ] as [String : Any]
        
        SVProgressHUD.show()
        print(url)
        print(parameter)
        
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                self.GoBack()
                let msg = getString(dic!["message"])
                self.showMyAlert(myMessage:msg)
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
        
    }
    func uploadImageInDatabase(image:UIImage)
    {
        
        let imageData = image.jpegData(compressionQuality: 0.5)
        let url = API.UploadImage
        //        let url = mainURL+URLS.ProfileImgUpload.rawValue
        print(url)
        let date = NSDate()
        let df = DateFormatter()
        df.dateFormat = "dd-mm-yy-hh-mm-ss"
        
        let imageName = df.string(from: date as Date)
        print(imageName)
        
        SVProgressHUD.show(withStatus: "Loading")
        AF.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(imageData!, withName: "image", fileName: "\(imageName).jpeg", mimeType: "image/jpeg")
        },
                  to: url, method: .post , headers:nil).response { resp in
                    switch resp.result {
                    case .success(let value):
                        if let data = value {
                            let resJson = JSON(data)
                            let dic1 = JSON(resJson)
                            print(dic1)
                            let status = resJson["status"].number
                            if status == 200{
                                
                                let dic = (dic1.dictionaryObject)as NSDictionary?
                                
                                self.imageUrl = getString(dic!["url"])
                                
                                
                                print(self.imageUrl)
                                SVProgressHUD.dismiss()
                                self.showMyAlert(myMessage: getString(dic!["message"]))
                                
                            }else{
                                print(data)
                                SVProgressHUD.dismiss()
                            }
                        }
                        
                    case .failure(let error):
                        SVProgressHUD.dismiss()
                        print(error)
                    }
                    
        }
        
        
        
    }
    
    func CallSkipUserApi() {
        
        let url =  API.SkipUser
        
        let parameter = [
            "device" : "iOS",
            "gcm_id" : FcmID
            ] as [String : Any]
        
        SVProgressHUD.show()
        print(url)
        print(parameter)
        
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                if self.video{
                    let token = getString(dic!["token"])
                    MyDefaults.set(true, forKey: .isSkip)
                    MyDefaults.set(token, forKey: .jwtToken)
                    let vc = self.storyboard?.instantiateViewController(identifier: "LiveVideoVC")as? LiveVideoVC
                    vc?.menuID = liveVideoMenusID
                    self.navigationController?.pushViewController(vc!, animated: true)
                }else{
                    let token = getString(dic!["token"])
                    MyDefaults.set(true, forKey: .isSkip)
                    MyDefaults.set(token, forKey: .jwtToken)
                    self.GoNext(screenName: "HomeVC")
                }
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }
    
}
