//
//  LoginVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 09/03/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class LoginVC: UIViewController {
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtPass: UITextField!
    
    @IBOutlet weak var btnCreateProfile: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var imgRememberMe: UIImageView!
    @IBOutlet weak var imgPassHide: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewRememberMe: UIView!
    @IBOutlet weak var viewPass: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnVideo: UIButton!
    var passHideShow = true
    var RememberMe = false
    var video = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
       
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter username or email", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtPass.attributedPlaceholder = NSAttributedString(string: "Enter password", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        loaddesign()
    }
    func loaddesign(){
        if liveVideoMenusStatus == false{
            btnVideo.isHidden = true
            imgVideo.isHidden = true
        }
        txtPass.tintColor = UIColor.white
        txtEmail.tintColor = UIColor.white
        btnLogin.layer.cornerRadius = 15
        btnLogin.clipsToBounds = true
        btnCreateProfile.layer.cornerRadius = 15
        btnCreateProfile.clipsToBounds = true
        viewName.layer.cornerRadius = 15
        viewName.clipsToBounds = true
        viewPass.layer.cornerRadius = 15
        viewPass.clipsToBounds = true
        viewRememberMe.layer.cornerRadius = 10
        imgRememberMe.image = UIImage(named: "")
        viewRememberMe.clipsToBounds = true
        txtPass.isSecureTextEntry = true
        
        let pass = UserDefaults.standard.string(forKey: "pass")
        let id = UserDefaults.standard.string(forKey: "id")
        if pass != ""{
            txtPass.text = pass
        }
        if id != ""{
            txtEmail.text = id
        }
    }
    
    @IBAction func btnHome(_ sender: Any) {
        video = false
        CallSkipUserApi()
        
    }
    
    @IBAction func btnLiveVideo(_ sender: Any) {
        video = true
        CallSkipUserApi()
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
        GoBack()
    }
    @IBAction func btnSkipCkick(_ sender: Any) {
        video = false
        CallSkipUserApi()
       
    }
    @IBAction func btnHideShowPassClick(_ sender: Any)
    {
        if passHideShow == true{
            passHideShow = false
            txtPass.isSecureTextEntry = false
            imgPassHide.image = UIImage(named: "hide_pass")
        }else{
            passHideShow = true
            txtPass.isSecureTextEntry = true
            imgPassHide.image = UIImage(named: "show_pass")
        }
        
    }
    
    @IBAction func btnTermsClick(_ sender: Any) {
        let obj = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        obj.PrivacyPolicy = false
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnPrivacyClick(_ sender: Any) {
        let obj = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        obj.PrivacyPolicy = true
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnRememberMeClick(_ sender: Any) {
        if RememberMe ==  true{
            imgRememberMe.image = UIImage(named: "")
            RememberMe = false
        }else{
            imgRememberMe.image = UIImage(named: "icn_Right")
            RememberMe = true
        }
    }
    @IBAction func btnLoginClick(_ sender: Any) {
        if validation(){
            CallLoginApi()
        }
    }
    @IBAction func btnForgotPassClicl(_ sender: Any) {
        
        GoNext(screenName: "ForgotPasswordVC")
    }
    @IBAction func btnCreateProfileClick(_ sender: Any) {
        
        GoNext(screenName: "CreateProfileVC")
    }
    func validation() -> Bool
    {
        if isEmptyString(txtEmail.text! as NSString)
        {
            self.showMyAlert(myMessage:MessageStrings().pleaseEnterEmailorUsername)
            return false
        }else if isEmptyString(txtPass.text! as NSString)
        {
            self.showMyAlert(myMessage:MessageStrings().pleaseEnterPassword)
            return false
        }
        return true
    }
}
extension LoginVC{
    func CallLoginApi() {
        let url =  API.login
        let check = validateEmailWithString(txtEmail.text! as NSString)
        var key = ""
        if check{
            key = "username"
        }else{
           key = "email"
        }
        let parameter = [key:txtEmail.text!,
                          "password": txtPass.text!,
                          "device":"iOS",
                          "fcm_id":FcmID
            ] as [String : Any]
        
        print(url)
        print(parameter)
        
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse1(dic: dic as! [String : Any]){
                if self.RememberMe == true{
                    UserDefaults.standard.set(self.txtEmail.text, forKey: "id")
                    UserDefaults.standard.set(self.txtPass.text, forKey: "pass")
                }
                let token = getString(dic!["token"])
                MyDefaults.set(true, forKey: .loginUser)
                MyDefaults.set(token, forKey: .jwtToken)
                self.GoNext(screenName: "HomeVC")
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }
    func CallSkipUserApi() {
        
        let url =  API.SkipUser
        
        let parameter = [
            "device" : "iOS",
            "fcm_id" : FcmID
            ] as [String : Any]
        
        SVProgressHUD.show()
        print(url)
        print(parameter)
        
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                if self.video{
                    let token = getString(dic!["token"])
                    MyDefaults.set(true, forKey: .isSkip)
                    MyDefaults.set(token, forKey: .jwtToken)
                    let vc = self.storyboard?.instantiateViewController(identifier: "LiveVideoVC")as? LiveVideoVC
                    vc?.menuID = liveVideoMenusID
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                }else{
                    let token = getString(dic!["token"])
                    MyDefaults.set(true, forKey: .isSkip)
                    MyDefaults.set(token, forKey: .jwtToken)
                    self.GoNext(screenName: "HomeVC")
                }
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }
    
}
