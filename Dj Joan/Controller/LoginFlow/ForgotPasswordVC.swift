//
//  ForgotPasswordVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 10/03/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class ForgotPasswordVC: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        loaddesign()
        
    }
    func loaddesign(){
        txtEmail.tintColor = UIColor.white
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter email address", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        btnSubmit.layer.cornerRadius = 15
        btnSubmit.clipsToBounds = true
        viewEmail.layer.cornerRadius = 15
        viewEmail.clipsToBounds = true
    }
    @IBAction func btnBackClick(_ sender: Any) {
         GoBack()
    }
  
    @IBAction func btnSubmitClick(_ sender: Any) {
        if validation(){
            CallForgotPasswordApi()
        }
    }
    func validation() -> Bool
    {
        if txtEmail.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterEmail)
            return false
        }
        else if validateEmailWithString(txtEmail.text! as NSString)
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterAValidEmail)
            return false
        }
        
        return true
    }
    
}
extension ForgotPasswordVC{
    func CallForgotPasswordApi() {
        
        let url = API.ForgotPassword
        
        let parameter = [ "email":txtEmail.text!
            ] as [String : Any]
        print(url)
        print(parameter)
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse1(dic: dic as! [String : Any]){
                 self.GoBack()
                
                let msg = getString(dic!["message"])
                self.showMyAlert(myMessage:msg)
               
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }
}
