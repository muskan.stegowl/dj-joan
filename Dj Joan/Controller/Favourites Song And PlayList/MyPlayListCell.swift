//
//  MyPlayListCell.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 07/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit

class MyPlayListCell: UITableViewCell {
@IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var imgPlayList: UIImageView!
    @IBOutlet weak var lblPlayListName: UILabel!
    @IBOutlet weak var btnDeletePlayList: UIButton!
    @IBOutlet weak var btnPlaylistClick: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        reload(false)
    }
    func reload(_ cellReload:Bool)
    {
        if cellReload == true
        {
            ViewContent.alpha = 0.7
            ViewContent.backgroundColor = MyColors.Red
        }
        else
        {
            ViewContent.alpha = 1
            ViewContent.backgroundColor = UIColor.clear
        }
    }
}
