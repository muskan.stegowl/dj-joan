//
//  FavouritSongVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 17/03/21.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
import PopupDialog
import SwiftGifOrigin
import GoogleMobileAds
class sponserCellFav: UICollectionViewCell {
    @IBOutlet weak var imgSponser: UIImageView!
}
class FavouritSongAndPlayListVC: AdsVC {
    @IBOutlet weak var colSlider: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewAddToFav: RoundView!
    @IBOutlet weak var imgHideShow: UIImageView!
    @IBOutlet weak var viewPlayer: FooterView!
    @IBOutlet weak var HightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viewPlayList: UIView!
    @IBOutlet weak var tblFavSong: UITableView!
    @IBOutlet weak var tblPlayList: UITableView!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var imgAdd: UIImageView!
    var arrSongList:[songListData] = []
    var arrPlayList:[PlayListData] = []
    var songID = 0
    var CurrentPage = 1
    var LastPage = 0
    var index = 0
    var Names = ""
    var count = 0
    var timerStop = Timer()
    var moveLeft:Bool = false
    var contentOffX:CGFloat = CGFloat()
    var cellSpacing: CGFloat = 0
    var lineSpacing: CGFloat = 0
    var arrSlider:[sponserData] = []
    var isPlayList = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colSlider.delegate = self
        colSlider.dataSource = self
        if isPlayList{
            tblFavSong.register(UINib(nibName:"MyPlayListCell", bundle: nil), forCellReuseIdentifier:"MyPlayListCell")
            lblAdd.text = "CLICK HERE ADD MUSIC TO YOUR PLAYLIST"
            imgAdd.image = UIImage(named: "icn_PlayList")
        }else{
            lblAdd.text = "CLICK HERE ADD MUSIC TO FAVORITES"
            imgAdd.image = UIImage(named: "icn_AddToFav")
            tblFavSong.register(UINib(nibName:"SongListCell", bundle: nil), forCellReuseIdentifier:"SongListCell")
        }
        
        tblPlayList.register(UINib(nibName:"PlayListCell", bundle: nil), forCellReuseIdentifier:"PlayListCell")
        tblFavSong.delegate = self
        tblFavSong.dataSource = self
        tblPlayList.delegate = self
        tblPlayList.dataSource = self
        txtSearch.tintColor = .black
        viewPlayList.isHidden = true
        viewAddToFav.isHidden = true
        
        WBGetSponserSlider()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewPlayer.commonInit()
        if miniStatus == .hide{
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }else{
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
            
        }
        if isPlayList{
            arrPlayList.removeAll()
            WBGetAndCreatePlaylist(type: "", playListName: "", index: 0)
        }else{
            
            arrSongList.removeAll()
            WBGetSongList()
        }
        
    }
    @IBAction func btnClosePlayList(_ sender: Any) {
        viewPlayList.isHidden =  true
    }
    @IBAction func btnHideShowView(_ sender: UIButton) {
        if miniStatus == .hide{
            miniStatus = .show
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            
            imgHideShow.image = UIImage(named: "icn_Down")
        }else{
            miniStatus = .hide
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }
    }
    @IBAction func btnSearch(_ sender: Any) {
        if isEmptyString(txtSearch.text! as NSString){
            showMyAlert(myMessage: "Please add search text.")
        }else{
            let vc = storyboard?.instantiateViewController(identifier: "SearchVC")as? SearchVC
            vc?.searchText = txtSearch.text!
            navigationController?.pushViewController(vc!, animated: false)
        }
    }
    @IBAction func btnAddToFav(_ sender: Any) {
        GoNext(screenName: "HomeVC")
        if isPlayList{
            
        }else{
            
        }
    }
    
    
    
}
extension FavouritSongAndPlayListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblFavSong{
            if isPlayList{
                return arrPlayList.count
            }else{
                return arrSongList.count
            }
            
        }else{
            return arrPlayList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblFavSong{
            if !isPlayList{
                let cell = tblFavSong.dequeueReusableCell(withIdentifier: "SongListCell", for: indexPath) as! SongListCell
                if  arrSongList[indexPath.row].song_name == "ads"{
                    if appdelegate.google_adsKey == ""{
                        cell.adsView.adUnitID = "ca-app-pub-4328731208314277/2632012147"
                    }else{
                        cell.adsView.adUnitID = appdelegate.google_adsKey
                    }
                    
                    cell.adsView.rootViewController = self
                    cell.adsView.load(GADRequest())
                    cell.adsView.delegate = self
                    cell.adsView.isHidden = false
                    cell.viewSong.isHidden = true
                }else{
                    cell.adsView.isHidden = true
                    cell.viewSong.isHidden = false
                    cell.lblSongName.text = arrSongList[indexPath.row].song_name
                    cell.lblArtistName.text = arrSongList[indexPath.row].song_artist
                    
                    let img = arrSongList[indexPath.row].song_image
                    cell.imgSong.sd_setImage(with: NSURL(string: (img!))! as URL,placeholderImage:UIImage(named: "icn_PlaceHolder"))
                    
                    if arrAllSong.count != 0{
                        if arrAllSong[songIndex].song_id == arrSongList[indexPath.row].song_id {
                            cell.imgGif.isHidden = true
                            cell.viewGIF.isHidden = false
                        }else{
                            cell.viewGIF.isHidden = true
                            cell.imgGif.isHidden = false
                        }
                    }else{
                        cell.viewGIF.isHidden = true
                        cell.imgGif.isHidden = false
                    }
                    cell.btnMore.tag = indexPath.row
                    cell.btnMore.addTarget(self, action: #selector(btnMoreClick), for: .touchUpInside)
                    cell.btnPlay.tag = indexPath.row
                    cell.btnPlay.addTarget(self, action: #selector(btnPlaySongClick), for: .touchUpInside)
                }
                return cell
            }else{
                let cell = tblFavSong.dequeueReusableCell(withIdentifier: "MyPlayListCell", for: indexPath) as! MyPlayListCell
                cell.lblPlayListName.text = arrPlayList[indexPath.row].playlist_name
                cell.btnPlaylistClick.tag = indexPath.row
                cell.btnPlaylistClick.addTarget(self, action: #selector(btnPlayListClick), for: .touchUpInside)
                cell.btnDeletePlayList.tag = indexPath.row
                cell.btnDeletePlayList.addTarget(self, action: #selector(btnDeleteClick), for: .touchUpInside)
                
                return cell
                
            }
        }else{
            let cell = tblPlayList.dequeueReusableCell(withIdentifier: "PlayListCell", for: indexPath) as! PlayListCell
            cell.lblName.text = arrPlayList[indexPath.row].playlist_name
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblPlayList{
            self.WBAddSonginPlaylist(playlistID:arrPlayList[indexPath.row].playlist_id!)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrSongList.count - 1 == indexPath.row{
            if LastPage > CurrentPage{
                let pagecount = CurrentPage + 1
                CurrentPage = pagecount
                if !isPlayList{
                    WBGetSongList()
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblPlayList{
            return 40
        }else{
            if isPlayList{
                return 50
            }else{
                return 60
            }
            
        }
        
        
    }
    @objc func btnMoreClick(_ sender: UIButton){
        if CheckConnection(){
            
            songID = arrSongList[sender.tag].song_id!
            let message = "Add Songs to Playlist or Favorites!"
            let popup = PopupDialog(title: title, message: message)
            self.present(popup, animated: true, completion: nil)
            index = sender.tag
            
            let ok = CancelButton(title:  "Add to Playlist") {
                print("Call API for Add to Playlist")
                
                self.popupAddtoplaylist()
                
            }
            
            let Delete = CancelButton(title: "Remove to Favorites") {
                print("Call API for Add to Favorites")
                
                //                if self.arrSongList[sender.tag].favourites_status!{
                //                    self.showMyAlert(myMessage: "Song already added in favourites.")
                //                }else{
                
                self.WBAddFavSong(SongID:self.arrSongList[sender.tag].song_id!, Index: sender.tag)
                //                }
                
            }
            
            popup.addButtons([ok,Delete])
        }
    }
    func popupAddtoplaylist() {
        let message = "Create Playlist or Add to Existing!"
        let popup = PopupDialog(title: title, message: message)
        self.present(popup, animated: true, completion: nil)
        
        let ok = CancelButton(title: "Create Playlist") {
            self.popupForPlaylist()
        }
        let Delete = CancelButton(title: "Add to Existing Playlist") {
            self.arrPlayList.removeAll()
            self.WBGetAndCreatePlaylist(type:"PlaylistList",playListName:"", index: 0)
            
        }
        popup.addButtons([ok,Delete])
    }
    func popupForPlaylist() {
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        
        // Create first button
        let buttonTwo = CancelButton(title: "Cancel", height: 60) {
        }
        
        // Create second button
        let buttonOne = DefaultButton(title: "Create", height: 60) {
            print(ratingVC.commentTextField.text!)
            self.Names = ratingVC.commentTextField.text!
            if self.Names != ""{
                self.WBGetAndCreatePlaylist(type:"PlaylistAdd",playListName:self.Names, index: 0)
            }else{
                self.showMyAlert(myMessage: "Enter Playlist Name")
            }
            
        }
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    @objc func btnPlaySongClick(_ sender: UIButton){
        let indexPath:IndexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell:SongListCell = tblFavSong.cellForRow(at: indexPath) as! SongListCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { (bool) in
            cell.reload(false)
            
            let vc = self.storyboard?.instantiateViewController(identifier: "PopUpPlayerVC")as? PopUpPlayerVC
            vc!.menuID = self.arrSongList[sender.tag].menu_id ?? 0
            vc!.songID = self.arrSongList[sender.tag].song_id ?? 0
            arrAllSong.removeAll()
            for item in self.arrSongList{
                arrAllSong.append(item)
            }
            songIndex = sender.tag
            playPauseSelected = 1
            MusicControll().playSongs(songIndex)
            self.tblFavSong.reloadData()
            self.navigationController?.pushViewController(vc!, animated: false)
        })
    }
    
    @objc func btnPlayListClick(_ sender: UIButton){
        let indexPath:IndexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell:MyPlayListCell = tblFavSong.cellForRow(at: indexPath) as! MyPlayListCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { (bool) in
            cell.reload(false)
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "SongListVC")as? SongListVC
                vc!.HTitle = self.arrPlayList[sender.tag].playlist_name!
                vc!.categoryID = self.arrPlayList[sender.tag].playlist_id!
                vc!.checkScreen = "playList"
                self.navigationController?.pushViewController(vc!, animated: false)
            } else {
                // Fallback on earlier versions
            }
            
        })
        
    }
    @objc func btnDeleteClick(_ sender: UIButton){
        self.showAlertWith_Cancel_CompletionChoice(title: "", message: "Are you sure you want to delete this playlist?") {
            self.WBGetAndCreatePlaylist(type: "PlaylistRemove", playListName:"", index: sender.tag)
        }
        
    }
}
extension FavouritSongAndPlayListVC{
    func WBGetSongList() {
        if CheckConnection(){
            var url = ""
            var parameter:[String:Any]
            parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
            ] as [String : Any]
            //            if checkScreen == "fav"{
            url = API.GetAndAddFav +  "FavouritesSongsList&limit=50&page=" + "\(CurrentPage)"
            //            }else if checkScreen == "playList"{
            //
            //                parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
            //                    ,"playlist_id":categoryID] as [String : Any]
            //                url = API.PlayListSong + "PlaylistSongsList&limit=50&page=" + "\(CurrentPage)"
            //            }else{
            //                if categoryID == 0{
            //                    url = API.MenuCategory + "\(menuID)" + "&menu_type=Tracks&limit=50&page=" + "\(CurrentPage)"
            //                }else{
            //                    url =  API.CategorySongAndVideo + "\(menuID)" + "&category_id=" + "\(categoryID)" + "&category_for=Tracks&limit=50&page=" + "\(CurrentPage)"
            //                }
            //            }
            
            print(url)
            
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { [self] (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = songListData(dict: item)
                            let dict = ["song_name":"ads","song_artist":"","song_image":""]
                            self.count = self.count + 1
                            if self.count == 8{
                                self.count = 0
                                let footer1 = songListData(dict: dict)
                                self.arrSongList.append(footer1)
                            }
                            self.arrSongList.append(footer)
                        }
                    }
                    self.viewAddToFav.isHidden = true
                    if self.arrSongList.count == 0 {
                        self.viewAddToFav.isHidden = false
                        self.showMyAlert(myMessage: "No Data Found")
                    }
                    self.CurrentPage = getInteger(apiResponse["current_page"])
                    self.LastPage = getInteger(apiResponse["last_page"])
                    self.tblFavSong.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBAddFavSong(SongID:Int,Index:Int) {
        if CheckConnection(){
            
            
            let url = API.GetAndAddFav + "AddRemoveSong"
            let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"song_id":SongID,"menu_id": arrSongList[Index].menu_id!] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    //                    if self.checkScreen == "fav"{
                    self.showMyAlert(myMessage: getString(dic!["message"]))
                    self.arrSongList.remove(at: Index)
                    self.tblFavSong.reloadData()
                    if self.arrSongList.count == 0{
                        self.viewAddToFav.isHidden = false
                    }
                    
                    //                    }else{
                    //                        self.showMyAlert(myMessage: getString(dic!["message"]))
                    //                        self.arrSongList[Index].favourites_status = true
                    //                    }
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetSponserSlider() {
        if CheckConnection(){
            let url = API.BannerSlider
            
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = sponserData(dict: item)
                            self.arrSlider.append(footer)
                            
                        }
                    }
                    self.pageControl.numberOfPages = self.arrSlider.count
                    self.pageControl.currentPage = 0
                    self.timerImages()
                    self.colSlider.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetAndCreatePlaylist(type:String,playListName:String,index:Int) {
        if CheckConnection(){
            
            var url = ""
            var parameter:[String:Any]
            
            if type == "PlaylistAdd"{
                url = API.GetAndCreatPlaylist + "PlaylistAdd"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_name":playListName,"menu_id": arrSongList[index].menu_id!]
            }else if type == "PlaylistRemove"{
                url = API.GetAndCreatPlaylist + "PlaylistRemove"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_id":arrPlayList[index].playlist_id as Any]
                
            }else{
                url = API.GetAndCreatPlaylist + "PlaylistList"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken)]
            }
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = PlayListData(dict: item)
                            self.arrPlayList.append(footer)
                        }
                        self.viewAddToFav.isHidden = true
                        if self.arrPlayList.count == 0 {
                            if self.isPlayList{
                                self.viewAddToFav.isHidden = false
                            }
                            self.showMyAlert(myMessage: "No Data Found")
                        }
                        if self.isPlayList{
                            self.tblFavSong.reloadData()
                        }else{
                            self.viewPlayList.isHidden = false
                            self.tblPlayList.reloadData()
                        }
                        
                    }else{
                        if self.isPlayList{
                            
                            let msg = getString(dic!["message"])
                            self.showMyAlert(myMessage: msg)
                            self.arrPlayList.remove(at: index)
                            self.tblFavSong.reloadData()
                            if self.arrPlayList.count == 0 {
                                self.viewAddToFav.isHidden = false
                            }
                        }else{
                            self.WBAddSonginPlaylist(playlistID:getInteger(dic!["playlist_id"]))
                        }
                        
                    }
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    
    func WBAddSonginPlaylist(playlistID:Int) {
        if CheckConnection(){
            
            
            let url = API.PlayListSong + "AddPlaylistSongs"
            
            let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_id":playlistID,"song_id":arrSongList[index].song_id!,"menu_id": arrSongList[index].menu_id!] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    self.showMyAlert(myMessage: getString(dic!["message"]))
                    
                    self.viewPlayList.isHidden = true
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
}
extension FavouritSongAndPlayListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrSlider.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (colSlider.frame.width), height: (colSlider.frame.height))
    }
    
    
    func  collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sponserCellFav", for: indexPath) as! sponserCellFav
        
        let img = arrSlider[indexPath.row].image
        Cell.imgSponser.sd_setImage(with:URL(string:img!),placeholderImage: UIImage(named: "icn_Slider"))
        
        //        Cell.imgSlider.image = UIImage(named: "icn_Slider")
        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let link =  arrSlider[indexPath.row].link
        
        if let url = URL(string: link!) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    //                UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                showMyAlert(myMessage: "Can't open website.")
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        
        let pageWidth = colSlider.frame.size.width
        let currentPage = colSlider.contentOffset.x / pageWidth
        
        if fmodf(Float(currentPage), 1.0) != 0.0{
            pageControl.currentPage = Int(currentPage) + 1
        }else{
            pageControl.currentPage = Int(currentPage)
        }
    }
    func timerImages()
    {
        
        Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
    }
    @objc func scrollToNextCell()
    {
        let pageWidth = colSlider.frame.size.width
        
        let currentPage = colSlider.contentOffset.x / pageWidth
        
        let cellSize = CGSize(width: self.view.frame.width,height: self.view.frame.height)
        let contentOffset = colSlider.contentOffset
        
        if (colSlider.contentSize.width <= colSlider.contentOffset.x + cellSize.width)
        {
            moveLeft = true
        }
        
        if contentOffset.x == 0
        {
            moveLeft = false
        }
        
        if moveLeft == true
        {
            pageControl.currentPage = Int(currentPage) - 1
            contentOffX = contentOffset.x - cellSize.width
            
            colSlider.scrollRectToVisible(CGRect(x: contentOffX,y: contentOffset.y,width: cellSize.width,height: cellSize.height), animated: true);
        }
        else
        {
            pageControl.currentPage = Int(currentPage) + 1
            contentOffX = contentOffset.x + cellSize.width
            
            colSlider.scrollRectToVisible(CGRect(x: contentOffX,y: contentOffset.y,width: cellSize.width,height: cellSize.height), animated: true);
        }
    }
    
}
