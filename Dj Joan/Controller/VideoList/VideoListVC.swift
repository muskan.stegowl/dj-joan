//
//  VideoListVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 12/03/21.
//

import UIKit
import AVKit
import AVFoundation
import SVProgressHUD
import MediaPlayer
class VideoListVC: AdsVC {
    @IBOutlet weak var viewHeader: HeaderView!
    @IBOutlet weak var imgHideShow: UIImageView!
    @IBOutlet weak var HightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var tblVideoList: UITableView!
    @IBOutlet weak var viewPlayer: FooterView!
    var CurrentPage = 1
    var LastPage = 0
    var menuID = 0
    var categoryID = 0
    var isFav = false
    var HTitle = ""
    var arrVideoList:[VideoListData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.lblTitle.text = HTitle
        tblVideoList.register(UINib(nibName:"tblVideoListCell", bundle: nil), forCellReuseIdentifier:"tblVideoListCell")
    
        tblVideoList.delegate = self
        tblVideoList.dataSource = self
        WBGetVideoList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewPlayer.commonInit()
        if miniStatus == .hide{
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }else{
            
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
        }
    }
    
    
    @IBAction func btnHideShowView(_ sender: UIButton) {
        if miniStatus == .hide{
            miniStatus = .show
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            
            imgHideShow.image = UIImage(named: "icn_Down")
        }else{
            miniStatus = .hide
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }
    }
}
extension VideoListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
            return arrVideoList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVideoList.dequeueReusableCell(withIdentifier: "tblVideoListCell", for: indexPath) as? tblVideoListCell
        

       
        cell?.lblVideoName.text = arrVideoList[indexPath.row].videos_name
        cell?.lblDate.text = arrVideoList[indexPath.row].date
        cell?.imgVideo.layer.cornerRadius = 20
        cell?.imgVideo.clipsToBounds = true
        cell?.ViewContent.layer.cornerRadius = 20
        let img = arrVideoList[indexPath.row].videos_image
        cell?.imgVideo.sd_setImage(with: NSURL(string: (img!))! as URL,placeholderImage:UIImage(named: "icn_VH"))
        cell?.btnPlayVideo.tag = indexPath.row
        cell!.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoClick), for: .touchUpInside)
        if isFav{
            cell?.viewDelete.isHidden = false
            cell?.btnDelete.tag = indexPath.row
            cell!.btnDelete.addTarget(self, action: #selector(btnDeleteVideoClick), for: .touchUpInside)
        }else{
            cell?.viewDelete.isHidden = true
        }
        
        return cell!

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrVideoList.count - 1 == indexPath.item{
            if LastPage > CurrentPage{
                let pagecount = CurrentPage + 1
                CurrentPage = pagecount
                WBGetVideoList()
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 140
    }
    
    @available(iOS 13.0, *)
    @objc func btnPlayVideoClick(_ sender: UIButton){
        
        let indexPath:IndexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell:tblVideoListCell = tblVideoList.cellForRow(at: indexPath) as! tblVideoListCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { (bool) in
            cell.reload(false)
            let vc = self.storyboard?.instantiateViewController(identifier: "VideoDetailVC")as? VideoDetailVC
            vc?.menuID = self.arrVideoList[indexPath.item].menu_id!
            if self.isFav{
                vc?.menuID = self.menuID
                vc!.isFav = true
            }
            arrAllVideo.removeAll()
            for item in self.arrVideoList{
                arrAllVideo.append(item)
            }
            videoIndex = sender.tag
//            MusicControll().PlayVideo(videoIndex)
            vc?.videoID = self.arrVideoList[indexPath.item].videos_id!
            self.navigationController?.pushViewController(vc!, animated: true)
            
        })
        
    }
    @objc func btnDeleteVideoClick(_ sender: UIButton){
        self.showAlertWith_Cancel_CompletionChoice(title: "", message: " Are you sure you want to remove this video from Favorities") {
            self.WBGetRemoveFaV(Index:sender.tag)
        }
        
        
    }
}
extension VideoListVC{
    func WBGetVideoList() {
        if CheckConnection(){
            
            var url = ""
            if isFav{
                url =  API.FavVideo + "FavouriteVideosList&limit=50&page="  + "\(CurrentPage)"
            }else{
                if categoryID != 0{
                    url =  API.CategorySongAndVideo + "\(menuID)" + "&category_id=" + "\(categoryID)" + "&category_for=Videos&limit=50&page=" + "\(CurrentPage)"
                }else{
                    url =  API.MenuCategory + "\(menuID)" + "&menu_type=Videos&limit=50&page=" + "\(CurrentPage)"
                }
            }
            
            
            
            
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
                ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = VideoListData(dict: item)
                            self.arrVideoList.append(footer)
                        }
                    }
                    if self.arrVideoList.count == 0 {
                        self.showMyAlert(myMessage: "No Data Found")
                    }
                    self.CurrentPage = getInteger(apiResponse["current_page"])
                    self.LastPage = getInteger(apiResponse["last_page"])
                    self.tblVideoList.reloadData()
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetRemoveFaV(Index:Int) {
        if CheckConnection(){
            
            let url = API.FavVideo + "AddRemoveFavouriteVideos"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),"menu_id":arrVideoList[Index].menu_id! ,"videos_id":arrVideoList[Index].videos_id as Any ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    let msg = getString(dic!["message"])
                    self.showMyAlert(myMessage: msg)
                    self.arrVideoList.remove(at: Index)
                    self.tblVideoList.reloadData()
                }
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
        }
    }
}
