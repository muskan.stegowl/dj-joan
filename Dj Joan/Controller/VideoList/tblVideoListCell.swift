//
//  tblVideoListCell.swift
//  Maiky Backstage
//
//  Created by Stegowl Macbook Pro on 08/06/21.
//

import UIKit

class tblVideoListCell: UITableViewCell {
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lblVideoName: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var viewDelete: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        reload(false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func reload(_ cellReload:Bool)
       {
           if cellReload == true
           {
               ViewContent.alpha = 0.7
               ViewContent.backgroundColor = MyColors.Red
           }
           else
           {
               ViewContent.alpha = 1
               ViewContent.backgroundColor = UIColor.clear
           }
       }
}
