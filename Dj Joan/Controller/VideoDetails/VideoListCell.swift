//
//  VideoListCell.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 09/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit

class VideoListCell: UICollectionViewCell {
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lblVideoName: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var viewDelete: UIView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var viewFooterHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        reload(false)
    }
    func reload(_ cellReload:Bool)
    {
        if cellReload == true
        {
            ViewContent.isHidden = false
            ViewContent.alpha = 0.7
            ViewContent.backgroundColor = MyColors.Red
            
        }
        else
        {
            ViewContent.isHidden = true
            ViewContent.alpha = 1
            ViewContent.backgroundColor = UIColor.clear
        }
    }
}
