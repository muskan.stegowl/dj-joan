//
//  VideoDetailVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 13/03/21.
//
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
import AVFoundation
import AVKit
class VideoDetailVC: AdsVC, AVPlayerViewControllerDelegate,UIWebViewDelegate{
    
    @IBOutlet weak var viewHeader: HeaderView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var colSuggesteVideo: UICollectionView!
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var colHeight: NSLayoutConstraint!
    @IBOutlet weak var VideoView: UIView!
    @IBOutlet weak var viewYouTube: UIView!
    
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    var playerController = AVPlayerViewController()
    var menuID = 0
    var videoID = 0
    var isFav = false
    var cellSpacing: CGFloat = 0
    var lineSpacing: CGFloat = 0
    var arrSuggestedVideos:[VideoListData] = []
    var viewWidth = 0.0
    var isReplay: Bool = false
    var isShow = false
    var isFull = false
    var paused: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        if arrAllVideo[videoIndex].url_type == 1{
            miniStatus = .hide
            currentPlayStatus = .none
            self.viewYouTube.isHidden = false
            self.VideoView.isHidden = true
            appdelegate.player.pause()
            appdelegate.playerItem = nil
            appdelegate.player.replaceCurrentItem(with: appdelegate.playerItem)
            appdelegate.player.pause()
            appdelegate.radioPlayer.pause()
            appdelegate.audioPlayer.pause()
            isRadio = false
            playPauseSelected = 0
            isLastPlay = ""
            UIWebView.loadRequest(self.webView)(URLRequest(url: URL(string:arrAllVideo[videoIndex].videos_link ?? "")!))
        }else{
            self.viewYouTube.isHidden = true
            self.VideoView.isHidden = false
            
//            arrAllVideo[videoIndex].videos_link = "https://stream4.xdevel.com/video0s976017-879/stream/playlist.m3u8"
            let urlString1 = URL(string:arrAllVideo[videoIndex].videos_link!)
            
            let videoAsset = AVAsset(url: urlString1!)
            
            let assetLength = Float(videoAsset.duration.value) / Float(videoAsset.duration.timescale)
            
            if assetLength == 0.0 {
                imgPlayer.image = UIImage(named: "icn_Offline")
            } else {
                MusicControll().PlayVideo(videoIndex)
                
                appdelegate.playerLayer.frame = VideoView.bounds
                appdelegate.playerLayer.videoGravity = .resizeAspectFill
                
                //       playerController = AVPlayerViewController()
                NotificationCenter.default.addObserver(self, selector: #selector(VideoDetailVC.didfinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: appdelegate.player.currentItem)
                playerController.player = appdelegate.player
                playerController.allowsPictureInPicturePlayback = true
                self.addChild(playerController)
                playerController.delegate = self
                playerController.view.frame = self.VideoView.frame
                if #available(iOS 14.2, *) {
                    playerController.canStartPictureInPictureAutomaticallyFromInline = true
                } else {
                    // Fallback on earlier versions
                }
                // Add sub view in your view
                self.VideoView.addSubview(playerController.view)
                //        playerController.player?.play()
                appdelegate.player.play()
                miniStatus = .show
                currentPlayStatus = .video
            }
            
        }
        //        self.present(playerController, animated: true, completion : nil)
        colSuggesteVideo.register(UINib(nibName: "VideoListCell", bundle: nil), forCellWithReuseIdentifier:"VideoListCell")
        colSuggesteVideo.delegate = self
        colSuggesteVideo.dataSource = self
        setCollectionViewLayout()
        WBGetVideoList()
        
        
        
        
        
    }
    
    @objc func didfinishPlaying(note : NSNotification)  {
        
        MusicControll().NextVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appdelegate.background_play()
        appdelegate.configurePlayer()
        playerController.allowsPictureInPicturePlayback = false
        if #available(iOS 14.2, *) {
            playerController.canStartPictureInPictureAutomaticallyFromInline = false
        } else {
            // Fallback on earlier versions
        }
        if appdelegate.player.rate == 1{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                appdelegate.player.pause()
                appdelegate.player.play()
                appdelegate.background_play()
                appdelegate.configurePlayer()
            }
        }
        
    }
    func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        print("present")
        let currentviewController = navigationController?.visibleViewController
        
        if currentviewController != playerViewController{
            self.addChild(playerController)
            playerController.delegate = self
            playerController.view.frame = self.VideoView.frame
            
            // Add sub view in your view
            self.VideoView.addSubview(playerController.view)
            appdelegate.player.pause()
            appdelegate.player.play()
            
            
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //      appdelegate.playerLayer.frame = VideoView.bounds
        //      appdelegate.playerLayer.videoGravity = .resizeAspectFill
        //      VideoView.layer.addSublayer(appdelegate.playerLayer)
        //        NotificationCenter.default.addObserver(self, selector: #selector(HidePlayer), name: NSNotification.Name(rawValue: "HidePIPMode"), object: nil)
        
    }
    @objc func HidePlayer(){
        appdelegate.playerLayer.frame = VideoView.bounds
        appdelegate.playerLayer.videoGravity = .resizeAspectFill
        
        //       playerController = AVPlayerViewController()
        NotificationCenter.default.addObserver(self, selector: #selector(VideoDetailVC.didfinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: appdelegate.player.currentItem)
        playerController.player = appdelegate.player
        playerController.allowsPictureInPicturePlayback = false
        self.addChild(playerController)
        playerController.delegate = self
        playerController.view.frame = self.VideoView.frame
        
        // Add sub view in your view
        self.VideoView.addSubview(playerController.view)
        
    }
    
    fileprivate func setCollectionViewLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let inset: CGFloat = 10
        layout.sectionInset = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
        self.cellSpacing = 8
        self.lineSpacing = 10
        layout.minimumLineSpacing = self.lineSpacing
        layout.minimumInteritemSpacing = self.cellSpacing
        layout.scrollDirection = .horizontal
        self.colSuggesteVideo.collectionViewLayout = layout
    }
    
    @IBAction func btnFavClick(_ sender: Any) {
        WBGetRemoveFaV()
    }
}
extension VideoDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSuggestedVideos.count
//        return arrSuggestedVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colSuggesteVideo.dequeueReusableCell(withReuseIdentifier: "VideoListCell", for: indexPath) as? VideoListCell
        cell?.lblVideoName.text = arrSuggestedVideos[indexPath.item].videos_name
        cell?.lblDate.text = arrSuggestedVideos[indexPath.item].date
        cell?.imgVideo.layer.cornerRadius = 10
        cell?.imgVideo.clipsToBounds = true
        cell?.ViewContent.layer.cornerRadius = 10
        let img = arrSuggestedVideos[indexPath.row].videos_image
        cell?.imgVideo.sd_setImage(with: NSURL(string: (img!))! as URL,placeholderImage:UIImage(named: "icn_PlaceHolderV"))
        cell?.btnPlayVideo.tag = indexPath.row
        cell!.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoClick), for: .touchUpInside)
        cell?.viewDelete.isHidden = true
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var cellInRow: CGFloat
        
        cellInRow = 2
        
        
        
        let width: CGFloat = (collectionView.bounds.width/cellInRow) - ((cellInRow-1)*self.cellSpacing)
        
        
        var height:CGFloat
        
        height = width * 1.6
        colHeight.constant = height + 40
        return CGSize(width: width, height: height)
        
    }
    
    @objc func btnPlayVideoClick(_ sender: UIButton){
        
        let indexPath:IndexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell:VideoListCell = colSuggesteVideo.cellForItem(at: indexPath) as! VideoListCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { [self] (bool) in
            cell.reload(false)
            self.videoID = self.arrSuggestedVideos[sender.tag].videos_id!
            arrAllVideo.removeAll()
            for item in self.arrSuggestedVideos{
                arrAllVideo.append(item)
            }
            videoIndex = sender.tag
            if arrAllVideo[videoIndex].url_type == 1{
                miniStatus = .hide
                currentPlayStatus = .none
                self.viewYouTube.isHidden = false
                self.VideoView.isHidden = true
                appdelegate.player.pause()
                appdelegate.playerItem = nil
                appdelegate.player.replaceCurrentItem(with: appdelegate.playerItem)
                appdelegate.player.pause()
                appdelegate.radioPlayer.pause()
                appdelegate.audioPlayer.pause()
                isRadio = false
                playPauseSelected = 0
                isLastPlay = ""
                UIWebView.loadRequest(self.webView)(URLRequest(url: URL(string:arrAllVideo[videoIndex].videos_link ?? "")!))
            }else{
                self.viewYouTube.isHidden = true
                self.VideoView.isHidden = false
                let urlString1 = URL(string:arrAllVideo[videoIndex].videos_link!)
                
                let videoAsset = AVAsset(url: urlString1!)
                
                let assetLength = Float(videoAsset.duration.value) / Float(videoAsset.duration.timescale)
                
                if assetLength == 0.0 {
                    imgPlayer.image = UIImage(named: "icn_Offline")
                } else {
                    
                    MusicControll().PlayVideo(videoIndex)
                    appdelegate.playerLayer.frame = self.VideoView.bounds
                    appdelegate.playerLayer.videoGravity = .resizeAspectFill
                    
                    //       playerController = AVPlayerViewController()
                    NotificationCenter.default.addObserver(self, selector: #selector(VideoDetailVC.didfinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: appdelegate.player.currentItem)
                    playerController.player = appdelegate.player
                    playerController.allowsPictureInPicturePlayback = true
                    self.addChild(playerController)
                    playerController.delegate = self
                    playerController.view.frame = self.VideoView.frame
                    if #available(iOS 14.2, *) {
                        playerController.canStartPictureInPictureAutomaticallyFromInline = true
                    } else {
                        // Fallback on earlier versions
                    }
                    // Add sub view in your view
                    self.VideoView.addSubview(playerController.view)
                    //        playerController.player?.play()
                    appdelegate.player.play()
                    miniStatus = .show
                    currentPlayStatus = .video
                }
            }
            
            self.arrSuggestedVideos.removeAll()
            self.WBGetVideoList()
            
        })
        
    }
    
}
extension VideoDetailVC{
    func WBGetVideoList() {
        if CheckConnection(){
            var url = ""
            var parameter:[String:Any]
            
            if isFav{
                url =  API.FavVideo + "FavouriteVideosDetails"
                
                parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),
                              
                              "videos_id":arrAllVideo[videoIndex].videos_id!
                ] as [String : Any]
            }else{
                if arrAllVideo[videoIndex].category_id == 0{
                    url =  API.SongAndVideoDetails + "Videos"
                    
                    parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),
                                  "menu_id":arrAllVideo[videoIndex].menu_id!,
                                  "videos_id":arrAllVideo[videoIndex].videos_id!
                    ] as [String : Any]
                }else{
                    url =  API.SongAndVideoDetails + "CategoryVideos"
                    
                    parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),
                                  "menu_id":arrAllVideo[videoIndex].menu_id!,
                                  "category_id":arrAllVideo[videoIndex].category_id!,
                                  "videos_id":arrAllVideo[videoIndex].videos_id!
                    ] as [String : Any]
                }
                
            }
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { [self] (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["suggested_data"] as? [[String : Any]] {
                        for item in data {
                            let footer = VideoListData(dict: item)
                            self.arrSuggestedVideos.append(footer)
                        }
                    }
                    if let data = apiResponse["videos_detail"] as? [[String : Any]] {
                        for item in data {
                            self.lblDesc.text = getString(item["videos_description"])
                            
                            viewHeader.lblTitle.text = getString(item["videos_name"])
                            let img = getString(item["videos_image"])
                            //                            self.imgVideo.sd_setImage(with: NSURL(string: (img))! as URL,placeholderImage:UIImage(named: "icn_PlaceHolderW"))
                            if getBool(item["favourites_status"]){
                                self.imgFav.image = UIImage(named: "icn_FavW")
                                self.imgFav.tintColor = UIColor(named:"MyTealColor")
                            }else{
                                self.imgFav.image = UIImage(named: "icn_FavW")
                                self.imgFav.tintColor = .white
                            }
                            
                        }
                    }
                    lblNoData.text = ""
                    if self.arrSuggestedVideos.count == 0 {
                        lblNoData.text = "No Suggested Videos Found. Check Back Later"
                        
                    }
                    
                    self.colSuggesteVideo.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetRemoveFaV() {
        if CheckConnection(){
            
            let url = API.FavVideo + "AddRemoveFavouriteVideos"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),"videos_id":arrAllVideo[videoIndex].videos_id!,"menu_id":arrAllVideo[videoIndex].menu_id!] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    let msg = getString(dic!["message"])
                    self.showMyAlert(myMessage: msg)
                    if getBool(dic!["favourites_status"]){
                        self.imgFav.image = UIImage(named: "icn_FavW")
                        self.imgFav.tintColor = UIColor(named:"MyTealColor")
                    }else{
                        self.imgFav.image = UIImage(named: "icn_FavW")
                        self.imgFav.tintColor = .white
                    }
                }
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
        }
    }
}

