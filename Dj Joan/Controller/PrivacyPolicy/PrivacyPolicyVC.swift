//
//  PrivacyPolicyVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 09/03/21.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
class PrivacyPolicyVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblText: UILabel!
    var PrivacyPolicy = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if PrivacyPolicy == true{
            lblTitle.text = "Privacy Policy"
        }else{
            lblTitle.text = "Terms and Conditions"
        }
        CallPrivacyPolicyApi()
        
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        GoBack()
    }
    func CallPrivacyPolicyApi() {
        
        let url = API.LegalDetails
        
        print(url)
        AFWrapper.requestGETURL(url,showLoader : true,enableJWT : false, headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                if self.PrivacyPolicy{
                    if let dic1 = dic!["privacy_data"]{
                        let array = dic1 as? NSArray
                        let arr = array![0] as? NSDictionary
                        self.lblText.attributedText = getString(arr!["description"]).html2AttributedString
                        self.lblText.textColor = UIColor.white
                        
                    }
                }else{
                    if let dic1 = dic!["terms_data"]{
                        let array = dic1 as? NSArray
                        let arr = array![0] as? NSDictionary
                        self.lblText.attributedText = getString(arr!["description"]).html2AttributedString
                        self.lblText.textColor = UIColor.white
                    }
                }
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }
}
