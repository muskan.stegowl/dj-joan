//
//  ChangePasswordVC.swift
//  Dj Kenny
//
//  Created by iOS TeamLead on 15/03/21.
//  Copyright © 2020 Stegowl. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
class ChangePasswordVC: UIViewController {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgCurrPass: UIImageView!
    @IBOutlet weak var imgConPass: UIImageView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var txtCurrentPass: UITextField!
    @IBOutlet weak var txtConPass: UITextField!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var imgNewPass: UIImageView!
    var ConPassHideShow = true
    var CurrPassHideShow = true
    var NewPassHideShow = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loaddesign()
    }
    
    func loaddesign(){
         txtCurrentPass.attributedPlaceholder = NSAttributedString(string: "Enter current password", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
         txtNewPass.attributedPlaceholder = NSAttributedString(string: "Enter new password", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
         txtConPass.attributedPlaceholder = NSAttributedString(string: "Enter confirm password", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtCurrentPass.tintColor = UIColor.white
        txtNewPass.tintColor = UIColor.white
        txtConPass.tintColor = UIColor.white
        btnSubmit.layer.cornerRadius = 15
        btnSubmit.clipsToBounds = true
        view1.layer.cornerRadius = 15
        view1.clipsToBounds = true
        view2.layer.cornerRadius = 15
        view2.clipsToBounds = true
        view3.layer.cornerRadius = 15
        view3.clipsToBounds = true
        txtConPass.isSecureTextEntry = true
        txtNewPass.isSecureTextEntry = true
        txtCurrentPass.isSecureTextEntry = true
    }
    @IBAction func btnLiveVideoClick(_ sender: Any) {
    }
    
    @IBAction func btnHomeClick(_ sender: Any) {
        
//        GoNext(screenName: "mainPlayerVC")
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
       GoBack()
    }
    
    @IBAction func btnCurrPassHS(_ sender: Any) {
        if CurrPassHideShow == true{
            CurrPassHideShow = false
            txtCurrentPass.isSecureTextEntry = false
            imgCurrPass.image = UIImage(named: "hide_pass")
        }else{
            CurrPassHideShow = true
            txtCurrentPass.isSecureTextEntry = true
            imgCurrPass.image = UIImage(named: "show_pass")
        }
    }
    @IBAction func btnNewPassHS(_ sender: Any) {
        if NewPassHideShow == true{
            NewPassHideShow = false
            txtNewPass.isSecureTextEntry = false
            imgNewPass.image = UIImage(named: "hide_pass")
        }else{
            NewPassHideShow = true
            txtNewPass.isSecureTextEntry = true
            imgNewPass.image = UIImage(named: "show_pass")
        }
    }
    @IBAction func btnConPassHS(_ sender: Any) {
        if ConPassHideShow == true{
            ConPassHideShow = false
            txtConPass.isSecureTextEntry = false
            imgConPass.image = UIImage(named: "hide_pass")
        }else{
            ConPassHideShow = true
            txtConPass.isSecureTextEntry = true
            imgConPass.image = UIImage(named: "show_pass")
        }
        
    }
    @IBAction func btnSubmitClick(_ sender: Any) {
        if validation(){
            CallChangePasswordApi()
        }
    }
    func validation() -> Bool
    {
        if txtCurrentPass.text == ""
        {
            showMyAlert(myMessage:"Please enter current password.")
            return false
        }
        else if txtNewPass.text == ""
        {
            showMyAlert(myMessage:"Please enter new password.")
            return false
        }
        else if txtCurrentPass.text == txtNewPass.text
        {
            
            showMyAlert(myMessage:"Current password and new password is a same.")
            return false
        }
        else if txtConPass.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterConfirmPassword)
            return false
        }
        else if txtNewPass.text != txtConPass.text{
            
            showMyAlert(myMessage:MessageStrings().ConfirmdPasswordNotMatch)
            return false
        }
        return true
    }
}
extension ChangePasswordVC{
    func CallChangePasswordApi() {
        let url = API.ChangePassword
        
        let parameter = [  "token":MyDefaults.getString(forKey: .jwtToken),
                           "current_password" :txtCurrentPass.text!,
                           "new_password" : txtNewPass.text!,
                           "confirm_password" : txtConPass.text!
            ] as [String : Any]
        print(parameter)
        print(url)
        
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                self.GoNext(screenName: "LoginVC")
                MyDefaults.signOut()
                let msg = getString(dic!["message"])
                self.showMyAlert(myMessage: msg)
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
        
    }
}
