//
//  NotificationVC.swift
//  Durisimo
//
//  Created by iOS TeamLead on 5/13/20.
//  Copyright © 2020 Stegowl02. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SVProgressHUD
class cellNoti: UITableViewCell {
    @IBOutlet weak var lblNoti: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}

class NotificationVC: AdsVC {
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var HeaderView: HeaderView!
    @IBOutlet weak var imgNoti: UIImageView!
    @IBOutlet weak var imgInsta: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    var CurrentPage = 1
    var LastPage = 0
    var arrNoti:[notiData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        HeaderView.lblTitle.text = "Notifications"
        tblNotification.register(UINib(nibName: "NotiCell", bundle: nil), forCellReuseIdentifier: "NotiCell")
        tblNotification.delegate = self
        tblNotification.dataSource = self
        WBGetNotifications()
    }
   
    
    
    func WBGetNotifications() {
        
        if CheckConnection(){
            let url =  API.Notifications + "\(CurrentPage)"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
                ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = notiData(dict: item)
                            self.arrNoti.append(footer)
                        }
                    }
                    if self.arrNoti.count == 0 {
                        self.showMyAlert(myMessage: "No Data Found")
                    }
                    self.CurrentPage = getInteger(apiResponse["current_page"])
                    self.LastPage = getInteger(apiResponse["last_page"])
                    self.tblNotification.reloadData()
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    
}
extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNoti.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblNotification.dequeueReusableCell(withIdentifier: "NotiCell", for: indexPath) as! NotiCell
        cell.lblTitle.text = arrNoti[indexPath.row].title
        cell.lblMsg.text = arrNoti[indexPath.row].message
        cell.lblTime.text = arrNoti[indexPath.row].date
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrNoti.count - 1 == indexPath.row{
            if LastPage > CurrentPage{
                let pagecount = CurrentPage + 1
                CurrentPage = pagecount
                WBGetNotifications()
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
