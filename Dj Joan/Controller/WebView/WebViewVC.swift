//
//  WebViewVC.swift
//  Luinny Corporan
//
//  Created by iOS TeamLead on 6/4/20.
//  Copyright © 2020 iOS TeamLead. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import WebKit
import SDWebImage
class WebViewVC:AdsVC{
    @IBOutlet weak var viewHeader: HeaderView!
    @IBOutlet weak var webViews: WKWebView!
    var url = ""
    var titleH = ""
    var type = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
            viewHeader.lblTitle.text = titleH
        webViews.navigationDelegate = self
        if type != ""{
            callInstaLivelApi()
        }else{
           let url1 = URL(string: url)!
            webViews.load(URLRequest(url: url1))
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Video Enter Full Screen:-
        NotificationCenter.default.addObserver(self, selector: #selector(self.VideoEnterFullScreen), name: UIWindow.didBecomeVisibleNotification, object: view.window)
        
        //Video Exit Full Screen:-
        NotificationCenter.default.addObserver(self, selector: #selector(self.VideoExitFullScreen), name: UIWindow.didBecomeHiddenNotification, object: view.window)
    }
    
    @objc func VideoExitFullScreen()
    {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "VideoStopped"), object: nil, userInfo: nil)
    }
    
    @objc func VideoEnterFullScreen()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "VideoStarted"), object: nil, userInfo: nil)
    }
    
    
}
extension WebViewVC : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
       SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
       SVProgressHUD.dismiss()
    }
}
extension WebViewVC{
    func callInstaLivelApi()
    {
        
        if CheckConnection(){
            var url = ""
            if type == "web"{
                url = API.DjDrakeLink
            }else{
                url = API.Support
            }
            
            
            let parameter = [
                "token":MyDefaults.getString(forKey: .jwtToken)
                ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                      if let data = apiResponse["data"] as? [[String : Any]] {
                let dic1 = data[0]as? NSDictionary
                        let url1 = URL(string: getString(dic1!["link"]))!
                        self.webViews.load(URLRequest(url: url1))
                    }
                   
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
        
    }
}

