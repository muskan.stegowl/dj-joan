//
//  IntroVC.swift
//  Dj Argenis
//
//  Created by Stegowl Macbook Pro on 23/10/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
class cellIntro: UICollectionViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewIntro: UIImageView!
}

class IntroVC: UIViewController {
    @IBOutlet weak var viewIntro: UIView!
    private var count = 0
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var colIntro: UICollectionView!
    var sliderArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
//        if UserDefaults.standard.bool(forKey: "Intro"){
//            viewIntro.isHidden = true
//            NextVC()
//        }else{
            viewIntro.isHidden = false
            colIntro.delegate = self
            colIntro.dataSource = self
            WBGetSliderData()
//        }
        
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        NextVC()
        
    }
    func NextVC(){
        if MyDefaults.getBool(forKey: .loginUser){
            self.GoNext(screenName:"HomeVC")
        }else{
            self.GoNext(screenName: "LoginVC")
        }
        UserDefaults.standard.set(true, forKey: "Intro")
    }
    @IBAction func btnNext(_ sender: Any) {
        if self.colIntro.numberOfItems(inSection: 0) > (self.count + 1) {
            var nextIndexPath = IndexPath(item: self.count, section: 0)
            nextIndexPath.item = self.count + 1
            if nextIndexPath.item == self.sliderArray.count - 1 {
                self.btnNext.setTitle("Enter", for: .normal)
            }else {
                self.btnNext.setTitle("Next", for: .normal)
            }
            self.colIntro.scrollToItem(at: nextIndexPath, at: .left, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.pageControl.currentPage = nextIndexPath.item
            }
        } else {
            NextVC()
        }
    }
}

//MARK:- CollectionView Methods
extension IntroVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = sliderArray.count
        return sliderArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.colIntro.dequeueReusableCell(withReuseIdentifier: "cellIntro", for: indexPath) as!cellIntro
        let dic = sliderArray[indexPath.row] as? NSDictionary
        let img = dic!["app_image"] as? String
        cell.lblTitle.text = dic!["app_title"] as? String
        cell.imgViewIntro.sd_setImage(with: URL(string:img ?? ""),placeholderImage: UIImage(named:"icn_PlaceHolder"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:(colIntro.frame.width), height: (colIntro.frame.height))
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        count = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if self.pageControl.currentPage == pageControl.numberOfPages - 1 {
            self.btnNext.setTitle("Enter", for: .normal)
        }else {
            self.btnNext.setTitle("Next", for: .normal)
        }
    }
}

//MARK:- API CALL
extension IntroVC {
    func WBGetSliderData() {
        
        let url = API.IntroSlider
        
        print(url)
        AFWrapper.requestGETURL(url,showLoader : true,enableJWT : false, headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                let data = dic!["app_data"] as! NSArray
                self.sliderArray = data.mutableCopy() as! NSMutableArray
                self.pageControl.currentPage = 0
                
                if self.sliderArray.count == 1{
                    self.btnNext.setTitle("Enter", for: .normal)
                }
                self.colIntro.reloadData()
            }
            
        }
        
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
    }
}
