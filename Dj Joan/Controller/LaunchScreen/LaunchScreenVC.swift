//
//  LaunchScreenVCViewController.swift

//
//  Created by Stegowl Macbook Pro on 09/03/21.
//

import UIKit
import Alamofire
import SVProgressHUD
class LaunchScreenVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        if CheckConnection(){
            PLAYWhenPhone()
            CallMenuStatusApi()
            
        }
        
    }
    func CallMenuStatusApi() {
        
        let url = API.MenuStatus
        
        print(url)
        AFWrapper.requestGETURL(url,showLoader : true,enableJWT : false, headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                if let data = apiResponse["livetv_menu_data"] as? [[String : Any]] {
                    for item in data {
                        liveVideoMenusStatus = getBool(item["visible_status"])
                        liveVideoMenusID = getInteger(item["menu_id"])
                    }
                }
                if let data = apiResponse["radio_menu_data"] as? [[String : Any]] {
                    for item in data {
                        radioMenusStatus = getBool(item["visible_status"])
                        radioMenusID = getInteger(item["menu_id"])
                    }
                }
                if UserDefaults.standard.bool(forKey: "Intro"){
                     if MyDefaults.getBool(forKey: .loginUser){
                         self.GoNext(screenName:"HomeVC")
                     }else{
                         self.GoNext(screenName: "LoginVC")
                     }
                }else{
                    self.GoNext(screenName:"IntroVC")
                }
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }

}
