//
//  PopUpPlayerVC.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 08/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
import PopupDialog
class PopUpPlayerVC: UIViewController{
    @IBOutlet weak var HeaderView: HeaderView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgSong: UIImageView!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var imgPre: UIImageView!
    @IBOutlet weak var imgPlayPause: UIImageView!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var lblFavCount: UILabel!
    @IBOutlet weak var imgPlayList: UIImageView!
    @IBOutlet weak var lblPlayListCount: UILabel!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var lblShareCount: UILabel!
    @IBOutlet weak var lblPlayCount: UILabel!
    @IBOutlet weak var lblhTitle: UILabel!
    @IBOutlet weak var tblPlayList: UITableView!
    @IBOutlet var viewPlaylist: UIView!
    var arrPlayList:[PlayListData] = []
    var songID = 0
    var menuID = 0
    var Names = ""
    var favStatus = false
     var PlayListStatus = false
    var count = 0
    var sID = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPlayList.register(UINib(nibName:"PlayListCell", bundle: nil), forCellReuseIdentifier:"PlayListCell")
        isFirstPlay = true
        isPlaySong = true
        isVideo = false
        isVideoPlay = false
        tblPlayList.delegate = self
               tblPlayList.dataSource = self
        WBGetSongDetails()
        viewPlaylist.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        setPlayPauseButton()
        UpdateControll()
       secondTimer.invalidate()
       secondTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.UpdateControll), userInfo: nil, repeats: true)
         NotificationCenter.default.addObserver(self, selector: #selector(WBGetSongDetails), name: NSNotification.Name(rawValue: "cellClick"), object: nil)
    }
    
    
    @IBAction func btnPreClick(_ sender: Any) {
        imgPre.image = UIImage(named: "icn_PreWP")
        imgPre.tintColor = UIColor(named: "MyTealColor")
         Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
             self.imgPre.image = UIImage(named: "icn_PreWP")
            self.imgPre.tintColor = .white
             MusicControll().previousSong()
             self.setPlayPauseButton()
             self.UpdateControll()
         }

    }
    
    @IBAction func btnPlayPauseClick(_ sender: Any) {
        if MusicControll().playPause() {
            setPlayPauseButton()
            UpdateControll()
        }
    }
    @IBAction func btnNextClick(_ sender: Any) {
        imgNext.image = UIImage(named: "icn_NextWP")
        imgNext.tintColor = UIColor(named: "MyTealColor")
        
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            self.imgNext.image = UIImage(named: "icn_NextWP")
            self.imgNext.tintColor = .white
            MusicControll().nextSong()
            self.setPlayPauseButton()
            self.UpdateControll()
        }
    }
    @IBAction func btnClosePlayList(_ sender: Any) {
        self.viewPlaylist.isHidden = true
    }
    @IBAction func btnFav(_ sender: Any) {
        WBAddFavSong()
    }
    @IBAction func btnPlayList(_ sender: Any) {
        popupAddtoplaylist()
    }
    func LoadImage(url:String){
        imgBG.sd_setImage(with: NSURL(string: url)! as URL, placeholderImage: UIImage(named:"icn_PlaceHolder"))
        
        imgSong.sd_setImage(with: NSURL(string: url)! as URL, placeholderImage: UIImage(named:"icn_PlaceHolder"))
        if imgSong.image == nil{
            print("NilImage")
        }else{
            
        let inputImage = CIImage(cgImage: (imgSong.image?.cgImage)!)
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(inputImage, forKey: "inputImage")
        filter?.setValue(10, forKey: "inputRadius")
        let blurred = filter?.outputImage
        
        var newImageSize: CGRect = (blurred?.extent)!
        newImageSize.origin.x += (newImageSize.size.width - (self.imgBG.image?.size.width)!) / 2
        newImageSize.origin.y += (newImageSize.size.height - (self.imgBG.image?.size.height)!) / 2
        newImageSize.size = (self.imgBG.image?.size)!
        
        let resultImage: CIImage = filter?.value(forKey: "outputImage") as! CIImage
        let context: CIContext = CIContext.init(options: nil)
        let cgimg: CGImage = context.createCGImage(resultImage, from: newImageSize)!
        let blurredImage: UIImage = UIImage.init(cgImage: cgimg)
        self.imgBG.image = blurredImage
        }
    }
    
    func popupAddtoplaylist() {
        let message = "Create Playlist or Add to Existing!"
        let popup = PopupDialog(title: title, message: message)
        self.present(popup, animated: true, completion: nil)
        
        let ok = CancelButton(title: "Create Playlist") {
            self.popupForPlaylist()
        }
        let Delete = CancelButton(title: "Add to Existing Playlist") {
            self.arrPlayList.removeAll()
            self.WBGetAndCreatePlaylist(type:"PlaylistList",playListName:"")
            
        }
        popup.addButtons([ok,Delete])
    }
    func popupForPlaylist() {
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        
        // Create first button
        let buttonTwo = CancelButton(title: "Cancel", height: 60) {
        }
        
        // Create second button
        let buttonOne = DefaultButton(title: "Create", height: 60) {
            print(ratingVC.commentTextField.text!)
            self.Names = ratingVC.commentTextField.text!
            if self.Names != ""{
                self.WBGetAndCreatePlaylist(type:"PlaylistAdd",playListName:self.Names)
            }else{
                self.showMyAlert(myMessage: "Enter Playlist Name")
            }
            
        }
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    @IBAction func btnShare(_ sender: Any) {
         WBShareSong()
        let song = arrAllSong[songIndex].song_name
        let artist = arrAllSong[songIndex].song_artist
        let url = arrAllSong[songIndex].song
        
        let songName:String = "Song Name: \(song ?? "")\n"
        let songUrl = "\nSong Url: \(url ?? "")\n"
        let artistName:String = "\nArtist Name: \(artist ?? "")\n"
        let AppName:String = "\nApp Name: By Dj Joan"
        
        displayShareSheet(songName,songUrl:songUrl,artistName:artistName ,appName:AppName)
    }
    @objc func UpdateControll() {
           if  appdelegate.audioPlayer.state != .stopped {
               lblSongName.text = arrAllSong[songIndex].song_name
               lblArtistName.text = arrAllSong[songIndex].song_artist
            HeaderView.lblTitle.text = arrAllSong[songIndex].song_name
                 setPlayPauseButton()
            if sID != arrAllSong[songIndex].song_id!{
                sID = arrAllSong[songIndex].song_id!
                let a = count + 1
                count = a
            }
           
            if count == 4{
                count = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    appdelegate.interstitial = appdelegate.reloadInterstitial()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    appdelegate.ShowAds()
                }
            }
           }
           
       }
    @objc func setPlayPauseButton() {
    if playPauseSelected == 1{
        imgPlayPause.image = UIImage(named: "icn_PauseR")
       
    }
    else{
        imgPlayPause.image = UIImage(named: "icn_PlayR")
        
    }
}
}
extension PopUpPlayerVC:UITableViewDelegate,UITableViewDataSource{
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        return arrPlayList.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tblPlayList.dequeueReusableCell(withIdentifier: "PlayListCell", for: indexPath) as! PlayListCell
        cell.lblName.text = arrPlayList[indexPath.row].playlist_name
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
        self.WBAddSonginPlaylist(playlistID:arrPlayList[indexPath.row].playlist_id!)
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
   
}
}
extension PopUpPlayerVC{
    @objc func WBGetSongDetails() {
        if CheckConnection(){
            let url =  API.SongAndVideoDetails + "Tracks"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
                ,"song_id":arrAllSong[songIndex].song_id!,"menu_id":arrAllSong[songIndex].menu_id!] as [String : Any]
            print(url)
            print(parameter)
            LoadImage(url:arrAllSong[songIndex].song_image!)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for result in data {
                        self.lblFavCount.text = getString(result["favourites_count"])
                        self.lblPlayCount.text = getString(result["total_played"])
                        self.lblPlayListCount.text = getString(result["playlist_status_count"])
                        self.lblShareCount.text = getString(result["total_shared"])
                        self.favStatus = getBool(result["favourites_status"])
                        self.PlayListStatus = getBool(result["playlist_status"])
                            if self.favStatus{
                                self.imgFav.image = UIImage(named: "icn_FavW")
                                self.imgFav.tintColor = UIColor(named:"MyTealColor")
                            }else{
                                self.imgFav.image = UIImage(named: "icn_FavW")
                                self.imgFav.tintColor = .white
                            }
                        if self.PlayListStatus{
                            self.imgPlayList.image = UIImage(named: "icn_PlayListW")
                            self.imgPlayList.tintColor = UIColor(named: "MyTealColor")
                        }else{
                            self.imgPlayList.image = UIImage(named: "icn_PlayListW")
                            self.imgPlayList.tintColor = .white
                        }
                        let shareStatus = getBool(result["total_shared_status"])
                        if shareStatus{
                             self.imgShare.image = UIImage(named: "icn_ShareSong")
                            self.imgShare.tintColor = UIColor(named: "MyTealColor")
                        }else{
                             self.imgShare.image = UIImage(named: "icn_ShareSong")
                            self.imgShare.tintColor = .white
                        }
                    }
                    }
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBAddFavSong() {
        if CheckConnection(){
            
            
            let url = API.GetAndAddFav + "AddRemoveSong"
            let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"song_id":arrAllSong[songIndex].song_id!,"menu_id": arrAllSong[songIndex].menu_id!] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    self.WBGetSongDetails()
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBShareSong() {
           if CheckConnection(){
               
               
               let url = API.TotalShared
               let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"song_id":arrAllSong[songIndex].song_id!] as [String : Any]
               
               print(url)
               print(parameter)
               AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                   print(apiResponse)
                   
                   let dic = apiResponse as NSDictionary?
                   if self.CheckResponse(dic: dic as! [String : Any]){
                       
                       self.WBGetSongDetails()
                   }
                   
               }
                   
               ){ (error) -> Void in
                   SVProgressHUD.dismiss()
                   print(error)
               }
               
           }
       }
    func WBAddSonginPlaylist(playlistID:Int) {
        if CheckConnection(){
            
            
            let url = API.PlayListSong + "AddPlaylistSongs"
            
            let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_id":playlistID,"song_id":arrAllSong[songIndex].song_id!,"menu_id": arrAllSong[songIndex].menu_id!] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    self.showMyAlert(myMessage: getString(dic!["message"]))
                    self.WBGetSongDetails()
                    self.viewPlaylist.isHidden = true
                    
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetAndCreatePlaylist(type:String,playListName:String) {
        if CheckConnection(){
            
            var url = ""
            var parameter:[String:Any]
            if type == "PlaylistAdd"{
                url = API.GetAndCreatPlaylist + "PlaylistAdd"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_name":playListName,"menu_id": arrAllSong[songIndex].menu_id!]
            }else{
                url = API.GetAndCreatPlaylist + "PlaylistList"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken)]
            }
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        
                        for item in data {
                            let footer = PlayListData(dict: item)
                            self.arrPlayList.append(footer)
                        }
                        if self.arrPlayList.count == 0 {
                            self.showMyAlert(myMessage: "No Data Found")
                        }
                        
                        self.viewPlaylist.isHidden = false
                        self.tblPlayList.reloadData()
                    }else{
                        
                        self.WBAddSonginPlaylist(playlistID:getInteger(dic!["playlist_id"]))
                    }
                    
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
}

