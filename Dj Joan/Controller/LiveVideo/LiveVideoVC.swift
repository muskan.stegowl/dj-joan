//
//  LiveVideoVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 10/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
import AVKit
import AVFoundation
import MediaPlayer
import Alamofire
import SwiftyJSON
import FirebaseDatabase
import FirebaseCore
class sponserCell: UICollectionViewCell {
    @IBOutlet weak var imgSponser: UIImageView!
}
class userMsgCell:UITableViewCell{
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
}
class adminMsgCell:UITableViewCell{
    @IBOutlet weak var imgAdmin: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
}
@available(iOS 13.0, *)
class LiveVideoVC: AdsVC,UITextViewDelegate, AVPlayerViewControllerDelegate {
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var viewSponserHeight: NSLayoutConstraint!
    @IBOutlet weak var colSponser: UICollectionView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var tblMessage: UITableView!
    @IBOutlet weak var imgPlayer: UIImageView!
    var ref : DatabaseReference?
    var timerStop = Timer()
    var moveLeft:Bool = false
    var menuID = 2
    var contentOffX:CGFloat = CGFloat()
    var commentList = [CommentListModel]()
    var arrSponser:[sponserData] = []
    var videoURL = ""
    var videoID = 0
    var UName = "User"
    var isFull = false
    var viewWidth = 0.0
    var isShow = false
    var playerController = AVPlayerViewController()
    var UImg = "http://3.237.236.131/djbabyface/assets/images/avtar.png"
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMessage.delegate = self
        txtMessage.text = "Post A Message"
        txtMessage.textColor = UIColor.darkGray
        txtMessage.tintColor = UIColor.black
        colSponser.delegate = self
        colSponser.dataSource = self
        
        viewSponserHeight.constant = colSponser.frame.width * 0.15
        tblMessage.delegate = self
        tblMessage.dataSource = self
        timerImages()
        ref = Database.database().reference()
        getAllComments()
        WBGetSponserSlider()
        
        //         secondTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.UpdateControll), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        appdelegate.background_play()
        appdelegate.configurePlayer()
        playerController.allowsPictureInPicturePlayback = false
        if #available(iOS 14.2, *) {
            playerController.canStartPictureInPictureAutomaticallyFromInline = false
        } else {
            // Fallback on earlier versions
        }
        if appdelegate.player.rate == 1{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                appdelegate.player.pause()
                appdelegate.player.play()
                appdelegate.background_play()
                appdelegate.configurePlayer()
            }
        }
        
    }
    func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        
        print("present")
        let currentviewController = navigationController?.visibleViewController
        
        if currentviewController != playerViewController{
            self.addChild(playerController)
            playerController.delegate = self
            playerController.view.frame = self.playerView.frame
            if #available(iOS 14.2, *) {
                playerController.canStartPictureInPictureAutomaticallyFromInline = false
            } else {
                // Fallback on earlier versions
            }
            playerController.allowsPictureInPicturePlayback = false
            // Add sub view in your view
            if #available(iOS 14.2, *) {
                playerController.canStartPictureInPictureAutomaticallyFromInline = true
            } else {
                // Fallback on earlier versions
            }
            playerController.allowsPictureInPicturePlayback = true
            self.playerView.addSubview(playerController.view)
            appdelegate.player.pause()
            appdelegate.player.play()
        }
    }
    @objc func UpdateControll() {
        
        if appdelegate.player.currentItem?.status == AVPlayerItem.Status.readyToPlay {
            print("Play")
            SVProgressHUD.dismiss()
        }else{
            SVProgressHUD.show()
        }
        if isLastPlay == "v"{
            MusicControll().videoRemoteControl()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtMessage.text == "Post A Message" {
            txtMessage.text = nil
            txtMessage.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtMessage.text.isEmpty {
            txtMessage.text = "Post A Message"
            txtMessage.textColor = UIColor.darkGray
        }
    }
    @IBAction func btnShare(_ sender: Any) {
        let songUrl : String = "Video Url: " + videoURL
        
        let AppName:String = "App Name: By Dj Joan"
        
        //        self.displayShareSheet(shareContent: (songName + "\n\n" + artistName + "\n\n" + songUrl + "\n\n" + AppName))
        self.displayShareSheet(shareContent: (songUrl + "\n\n" + AppName))
        
    }
    func displayShareSheet(shareContent:String) {
        
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        
        present(activityViewController, animated: true, completion: {})
        
    }
    @IBAction func btnLikeDisLike(_ sender: Any) {
        WBGetVideoLikeDisLike()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        currentMenu = .home
        GoNext(screenName: "HomeVC")
    }
    @IBAction func btnPostMsg(_ sender: Any) {
        if txtMessage.text == ""{
            showMyAlert(myMessage: "Please enter message.")
        }else if txtMessage.text == "Post A Message"{
            showMyAlert(myMessage: "Please enter message.")
        }else{
            let dict = NSMutableDictionary()
            
            dict["commentImageURL"] = UImg
            
            dict["comment"] = txtMessage.text
            dict["commentByName"] = UName
            
            dict["type"] = "user"
            //            dict["type"] = "admin"
            let dateFormattor = DateFormatter()
            dateFormattor.timeZone = NSTimeZone(name: "GMT-4") as TimeZone?
            dateFormattor.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormattor.string(from: Date())
            dict["commentDate"] = myDate
            dateFormattor.dateFormat = "h:mm a"
            let myDate1 = dateFormattor.string(from: Date())
            dict["time"] = myDate1
            
            ref?.child("User").childByAutoId().setValue(dict)
            self.txtMessage.text = ""
        }
        
    }
    
}
@available(iOS 13.0, *)
extension LiveVideoVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrSponser.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (colSponser.frame.width), height: (colSponser.frame.height))
    }
    
    
    func  collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sponserCell", for: indexPath) as! sponserCell
        
        let img = arrSponser[indexPath.row].image
        
        Cell.imgSponser.sd_setImage(with: NSURL(string: (img!))! as URL)
        
        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let link =  arrSponser[indexPath.row].link
        
        if let url = URL(string: link!) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    //                UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                showMyAlert(myMessage: "Can't open website.")
            }
        }
        
        
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        
        let pageWidth = colSponser.frame.size.width
        let currentPage = colSponser.contentOffset.x / pageWidth
        
        //        if fmodf(Float(currentPage), 1.0) != 0.0{
        //            pageControl.currentPage = Int(currentPage) + 1
        //        }else{
        //            pageControl.currentPage = Int(currentPage)
        //        }
    }
    func timerImages()
    {
        
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
    }
    @objc func scrollToNextCell()
    {
        let pageWidth = colSponser.frame.size.width
        //        let pageHeight = collectionSlider.frame.size.height
        //         let currentPage = collectionSlider.contentOffset.y / pageHeight
        let currentPage = colSponser.contentOffset.x / pageWidth
        
        let cellSize = CGSize(width: self.view.frame.width,height: self.view.frame.height)
        let contentOffset = colSponser.contentOffset
        
        if (colSponser.contentSize.width <= colSponser.contentOffset.x + cellSize.width)
        {
            moveLeft = true
        }
        
        if contentOffset.x == 0
        {
            moveLeft = false
        }
        
        if moveLeft == true
        {
            // pageControl.currentPage = Int(currentPage) - 1
            contentOffX = contentOffset.x - cellSize.width
            
            colSponser.scrollRectToVisible(CGRect(x: contentOffX,y: contentOffset.y,width: cellSize.width,height: cellSize.height), animated: true);
        }
        else
        {
            // pageControl.currentPage = Int(currentPage) + 1
            contentOffX = contentOffset.x + cellSize.width
            
            colSponser.scrollRectToVisible(CGRect(x: contentOffX,y: contentOffset.y,width: cellSize.width,height: cellSize.height), animated: true);
        }
    }
    
}
@available(iOS 13.0, *)
extension LiveVideoVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic = commentList[indexPath.row]
        let name = dic.type
        if name != "user"{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "adminMsgCell")as? adminMsgCell
            cell?.lblTime.text = dic.time
            cell?.lblComment.text = dic.comment
            let img = dic.commentImageURL
            cell?.imgAdmin.sd_setImage(with: (NSURL(string: (img)) as! URL),placeholderImage:UIImage(named: "icn_MenuHeader"))
            cell?.imgAdmin.layer.cornerRadius = 20
            return cell!
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "userMsgCell")as? userMsgCell
            cell?.lblTime.text = dic.time
            cell?.lblComment.text = dic.comment
            
            let img = dic.commentImageURL
            cell?.imgUser.sd_setImage(with: (NSURL(string: (img)) as! URL),placeholderImage:UIImage(named: "icn_User"))
            cell?.lblUserName.text = dic.commentByName
            cell?.imgUser.layer.cornerRadius = 20
            return cell!
        }
    }
    
    
}
//Mark: Real time chat
@available(iOS 13.0, *)
extension LiveVideoVC{
    func getAllComments() {
        _ = ref?.observe(.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            print(postDict)
            
            if let dataForKey = postDict["User"] {
                
                let data = dataForKey as! [String:Any]
                self.commentList.removeAll()
                for key in data.keys {
                    print(key)
                    let tempDict = CommentListModel(dictionary: data["\(key)"] as! NSDictionary)
                    let date = tempDict?.commentDate.toDate()
                    tempDict?.date = date
                    
                    tempDict?.commentDate = date?.toString() ?? ""
                    self.commentList.append(tempDict ?? CommentListModel())
                    //                    commentList.insert(tempDict ?? CommentListModel(), at: 0)
                }
                self.commentList = self.commentList.sorted(by: { $0.date ?? Date() < $1.date ?? Date()})
                print(self.commentList)
                
                
                self.tblMessage.reloadData()
                self.tblMessage.scrollToRow(at: IndexPath(row: self.commentList.count - 1, section: 0), at: .bottom, animated: false)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.tblMessage.scrollToRow(at: IndexPath(row: self.commentList.count - 1, section: 0), at: .bottom, animated: true)
                }
            }
        })
    }
    func WBGetSponserSlider() {
        if CheckConnection(){
            let url = API.SponsorSlider
            
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : false,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                self.WBGetVideo(isLoader: true)
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = sponserData(dict: item)
                            self.arrSponser.append(footer)
                        }
                    }
                    self.colSponser.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetVideo(isLoader: Bool) {
        if CheckConnection(){
            
            let url =  API.MenuCategory + "\(menuID)" + "&menu_type=Videos"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
            ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : isLoader,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { [self] (apiResponse) in
                print(apiResponse)
                if MyDefaults.getBool(forKey: .loginUser){
                    self.CallGetProfileApi()
                }
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            self.videoID = getInteger(item["videos_id"])
                            self.videoURL = getString(item["videos_link"])
                            appdelegate.playerLayer.frame = self.playerView.bounds
                            appdelegate.playerLayer.videoGravity = .resizeAspectFill
                            self.playerView.layer.addSublayer(appdelegate.playerLayer)
                            
                            arrAllVideo.removeAll()
                            let footer = VideoListData(dict: item)
                            arrAllVideo.append(footer)
                            videoIndex = 0
                            //                            arrAllVideo[0].videos_link = "https://stream4.xdevel.com/video0s976017-879/stream/playlist.m3u8"
                            
                            let urlString1 = URL(string:arrAllVideo[videoIndex].videos_link!)
                            
                            let videoAsset = AVAsset(url: urlString1!)
                            
                            let assetLength = Float(videoAsset.duration.value) / Float(videoAsset.duration.timescale)
                            
                            if assetLength == 0.0 {
                                imgPlayer.image = UIImage(named: "icn_Offline")
                            } else {
                                MusicControll().PlayVideo(videoIndex)
                                appdelegate.playerLayer.frame = self.playerView.bounds
                                appdelegate.playerLayer.videoGravity = .resizeAspectFill
                                
                                self.playerController.player = appdelegate.player
                                self.playerController.allowsPictureInPicturePlayback = true
                                self.addChild(playerController)
                                playerController.delegate = self
                                playerController.view.frame = self.playerView.frame
                                if #available(iOS 14.2, *) {
                                    playerController.canStartPictureInPictureAutomaticallyFromInline = true
                                } else {
                                    // Fallback on earlier versions
                                }
                                // Add sub view in your view
                                
                                
                                self.playerView.addSubview(playerController.view)
                                appdelegate.player.play()
                                
                            }
                            
                            self.lblLikeCount.text = "\(getInteger(item["video_like_count"]))"
                            if getBool(item["video_like_status"]){
                                self.imgLike.image = UIImage(named:"icn_LikeR")
                                self.imgLike.tintColor = .MyTealColor
                            }else{
                                self.imgLike.image = UIImage(named: "icn_LikeW")
                            }
                        }
                    }
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBGetVideoLikeDisLike() {
        if CheckConnection(){
            
            let url =  API.VideoLikeDisLike
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),"videos_id":videoID
            ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    
                    self.lblLikeCount.text = "\(getInteger(dic!["videos_like_count"]))"
                    if getBool(dic!["videos_like_status"]){
                        self.imgLike.image = UIImage(named:"icn_LikeR")
                        self.imgLike.tintColor = .MyTealColor
                    }else{
                        self.imgLike.image = UIImage(named: "icn_LikeW")
                    }
                    self.showMyAlert(myMessage: getString(dic!["message"]))
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func CallGetProfileApi() {
        
        
        let url = API.ProfileDetails
        
        let parameter = [
            "token":MyDefaults.getString(forKey: .jwtToken)
        ] as [String : Any]
        print(parameter)
        print(url)
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                
                if let data = apiResponse["data"] as? [[String : Any]] {
                    for item in data {
                        self.UName = getString(item["name"])
                        if getString(item["image"]) != ""{
                            self.UImg = getString(item["image"])
                        }
                    }
                }
                
            }
            
        }
        
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
        
    }
}
