//
//  FooterView.swift
//  Gems
//
//  Created by Stegowl's Macbook on 26/12/20.
//  Copyright © 2020 Stegowl's Macbook. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SVProgressHUD
import StreamingKit
class FooterView: UIView,STKAudioPlayerDelegate {
    func audioPlayer(_ audioPlayer: STKAudioPlayer, unexpectedError errorCode: STKAudioPlayerErrorCode) {
        print("\(errorCode)")
    }
    
    
    
    let nibName = "FooterView"
    var contentView:UIView?
    var playerLoaderHide = 0
    
    @IBOutlet weak var viewSongPlayer: UIView!
    
    @IBOutlet weak var slbSlider: UISlider!
    @IBOutlet weak var imgShuffle: UIImageView!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var lblTotalTIme: UILabel!
    @IBOutlet weak var lblCurrentTIme: UILabel!
    @IBOutlet weak var imgPreSong: UIImageView!
    @IBOutlet weak var imgNextSong: UIImageView!
    @IBOutlet weak var imgRepeat: UIImageView!
    @IBOutlet weak var imgPlayPauseSong: UIImageView!
    
    @IBOutlet weak var imgVideoPlayPause: UIImageView!
    @IBOutlet weak var imgSound: UIImageView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var img_PreV: UIImageView!
    @IBOutlet weak var imgNextV: UIImageView!
    @IBOutlet weak var viewPlayer: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    override func reloadInputViews() {
        print("")
    }
    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        if currentPlayStatus == .video{
            viewVideo.isHidden = false
            viewSongPlayer.isHidden = true
            if isMute{
                imgSound.image = UIImage(named: "icn_Mute")
            }else{
                imgSound.image = UIImage(named: "icn_UnMute")
            }
            if appdelegate.player.rate != 0{
                imgVideoPlayPause.image = UIImage(named: "icn_PauseV")
            }else{
                imgVideoPlayPause.image = UIImage(named: "icn_PlayV")
            }
            LoadVideoPlayer()
        }else{
            viewSongPlayer.isHidden = false
            viewVideo.isHidden = true
            setPlayPauseButton()
            
        }
        let SwipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        SwipeUp.direction = UISwipeGestureRecognizer.Direction.up
        self.viewSongPlayer.addGestureRecognizer(SwipeUp)
        
       
        UpdateControll()
        contentView = view
        //        slbSlider.minimumValue = 0
        slbSlider.isContinuous = false
        let thumbImage = UIImage(named: "icn_Seek")
        slbSlider.setThumbImage(thumbImage, for: .normal)
        secondTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.UpdateControll), userInfo: nil, repeats: true)
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped Left")
                
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
                if let vc = UIApplication.topViewController(){
                    vc.presentVC(identifire:"PopUpPlayerVC")
                }
            default:
                break
            }
        }
    }
    
    func LoadVideoPlayer(){
        appdelegate.playerLayer.removeFromSuperlayer()
        appdelegate.playerLayer.frame = viewPlayer.frame
        appdelegate.playerLayer.videoGravity = .resizeAspectFill
        viewPlayer.layer.addSublayer(appdelegate.playerLayer)
    }
    @objc func playerDidFinishPlaying(sender: Notification) {
        print("Next")
        MusicControll().NextVideo()
    }
    
    @IBAction func btnPreVideo(_ sender: Any) {
        img_PreV.image = UIImage(named: "icn_PreVW")
        img_PreV.tintColor = UIColor(named: "MyTealColor")
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            self.img_PreV.image = UIImage(named: "icn_PreVW")
            self.img_PreV.tintColor = .white
            MusicControll().PreviousVideo()
        }
    }
    @IBAction func btnNextVideo(_ sender: Any) {
        imgNextV.image = UIImage(named: "icn_NextVW")
        imgNextV.tintColor = UIColor(named: "MyTealColor")
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            self.imgNextV.image = UIImage(named: "icn_NextVW")
            self.img_PreV.tintColor = .white
            MusicControll().NextVideo()
        }
    }
    @IBAction func btnSound(_ sender: Any) {
        if isMute{
            isMute = false
            appdelegate.player.isMuted = false
            imgSound.image = UIImage(named: "icn_UnMute")
        }else{
            appdelegate.player.isMuted = true
            isMute = true
            imgSound.image = UIImage(named: "icn_Mute")
            
        }
    }
    @IBAction func btnVideoPlayPause(_ sender: Any) {
        appdelegate.radioPlayer.pause()
        appdelegate.audioPlayer.pause()
        isRadio = false
        playPauseSelected = 0
        isLastPlay = "v"
        if isVideoPlay{
            isVideoPlay = false
            appdelegate.player.pause()
            imgVideoPlayPause.image = UIImage(named: "icn_PlayV")
        }else{
            isVideoPlay = true
            appdelegate.player.play()
            imgVideoPlayPause.image = UIImage(named: "icn_PauseV")
        }
        
    }
    
    @IBAction func songSlider(_ sender: Any) {
        if appdelegate.audioPlayer.state == STKAudioPlayerState.playing
        {
            print("Slider Changed : \(slbSlider.value)")
            appdelegate.audioPlayer.seek(toTime: Double(slbSlider.value))
            //UpdateControll()
            MusicControll().remoteControl()
        }
    }
    
    @IBAction func btnShuffle(_ sender: Any) {
        if globalShuffle == 1{
            globalShuffle = 0
        }else{
            globalShuffle = 1
        }
        setShuffleButton()
    }
    
    @IBAction func btnRepeat(_ sender: Any) {
        if globalRepeat == 1{
            globalRepeat = 0
        }else{
            globalRepeat = 1
        }
        setRepeatButton()
    }
    @IBAction func btnPreSong(_ sender: Any) {
        imgPreSong.image = UIImage(named: "previous")
        self.imgPreSong.tintColor = UIColor(named: "MyTealColor")
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            self.imgPreSong.image = UIImage(named: "previous")
            self.imgPreSong.tintColor = .white
            MusicControll().previousSong()
            self.lblSongName.text = arrAllSong[songIndex].song_name
            self.lblArtistName.text = arrAllSong[songIndex].song_artist
            self.slbSlider.minimumValue = 0
        }
        
    }
    @IBAction func btnNextSong(_ sender: Any) {
        imgNextSong.image = UIImage(named: "next")
        self.imgNextSong.tintColor = UIColor(named: "MyTealColor")
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            self.imgNextSong.image = UIImage(named: "next")
            self.imgNextSong.tintColor = .white
            MusicControll().nextSong()
            self.lblSongName.text = arrAllSong[songIndex].song_name
            self.lblArtistName.text = arrAllSong[songIndex].song_artist
            self.slbSlider.minimumValue = 0
        }
    }
    @IBAction func btnPlayPauseSongClick(_ sender: Any) {
        if MusicControll().playPause() {
            setPlayPauseButton()
            self.lblSongName.text = arrAllSong[songIndex].song_name
            self.lblArtistName.text = arrAllSong[songIndex].song_artist
            MusicControll().remoteControl()
        }
    }
    @objc func setShuffleButton() {
        if globalShuffle == 1{
            imgShuffle.image = UIImage(named:"icn_shuffle")
            imgShuffle.tintColor = UIColor(named: "MyTealColor")
        }
        else{
            
            imgShuffle.image = UIImage(named:"icn_shuffle")
            imgShuffle.tintColor = .white
            
        }
    }
    
    @objc func setRepeatButton() {
        if globalRepeat == 1{
            
            imgRepeat.image = UIImage(named:"icn_Repeat")
            imgRepeat.tintColor = UIColor(named: "MyTealColor")
        }
        else{
            
            imgRepeat.image = UIImage(named:"icn_Repeat")
            imgRepeat.tintColor = .white
        }
        
    }
    @objc func setPlayPauseButton() {
        if playPauseSelected == 1{
            imgPlayPauseSong.image = UIImage(named: "pause")
            imgPlayPauseSong.tintColor = .white
        }else{
            imgPlayPauseSong.image = UIImage(named: "play")
            imgPlayPauseSong.tintColor = .white
        }
        
    }
    
    @objc func UpdateControll() {
        //appDelegate.background_play()
        if appdelegate.player.rate != 0{
            imgVideoPlayPause.image = UIImage(named: "icn_PauseV")
        }else{
            imgVideoPlayPause.image = UIImage(named: "icn_PlayV")
        }
        if  appdelegate.audioPlayer.state != STKAudioPlayerState.stopped {
            slbSlider.maximumValue = Float(appdelegate.audioPlayer.duration)
            slbSlider.value = Float(appdelegate.audioPlayer.progress)
            self.lblCurrentTIme.text = (self.formatTimeFromSeconds(totalSeconds: Int(appdelegate.audioPlayer.progress)))
            self.lblTotalTIme.text = "\(self.formatTimeFromSeconds(totalSeconds: Int(appdelegate.audioPlayer.duration)))"
        }
        if isLastPlay == "v"{
            MusicControll().videoRemoteControl()
        }
        
//        print("Live stream \(self.formatTimeFromSeconds(totalSeconds: Int(appdelegate.audioPlayer.progress)))")
//        print("Total Time \(self.formatTimeFromSeconds(totalSeconds: Int(appdelegate.audioPlayer.duration)))")
        setPlayPauseButton()
        if appdelegate.audioPlayer.state == STKAudioPlayerState.paused{
            self.lblSongName.text = arrAllSong[songIndex].song_name
            self.lblArtistName.text = arrAllSong[songIndex].song_artist
            
        }
        if appdelegate.audioPlayer.state == STKAudioPlayerState.stopped{
            //self.appDelegate.secondTimer.invalidate()
        }
        if appdelegate.audioPlayer.state == STKAudioPlayerState.buffering
        {
            print(appdelegate.audioPlayer.state == STKAudioPlayerState.buffering ? "buffering" : "")
            SVProgressHUD.show(withStatus: "LOADING")
            //btnPlayPause.isSelected = false
            //playPauseSelected = 0
            playerLoaderHide = 1
        }
        else
        {
            if playerLoaderHide == 1
            {
                
                playerLoaderHide = 0
                print("Dismiss")
                SVProgressHUD.dismiss()
                playPauseSelected = 1
                appdelegate.commandCenter.previousTrackCommand.isEnabled = true
                appdelegate.commandCenter.playCommand.isEnabled = true
                appdelegate.commandCenter.pauseCommand.isEnabled = true
                appdelegate.commandCenter.nextTrackCommand.isEnabled = true
                appdelegate.commandCenter.changePlaybackRateCommand.isEnabled = true
                //btnPlayPause.isSelected = true
            }
        }
        
        if appdelegate.audioPlayer.state == STKAudioPlayerState.playing {
           
            MusicControll().remoteControl()
            if appdelegate.audioPlayer.progress >= appdelegate.audioPlayer.duration-1 || appdelegate.audioPlayer.progress >= appdelegate.audioPlayer.duration-2{
                if globalRepeat == 1{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        MusicControll().playSongs(songIndex)
                        self.setPlayPauseButton()
                        self.lblSongName.text = arrAllSong[songIndex].song_name
                        self.lblArtistName.text = arrAllSong[songIndex].song_artist
                    }
                }else{
                    
                    if  globalShuffle == 1{
                        let randomSongsIndex = Int(arc4random_uniform(UInt32(arrAllSong.count)))
                        songIndex = randomSongsIndex
                        MusicControll().playSongs(songIndex)
                        self.setPlayPauseButton()
                        self.lblSongName.text = arrAllSong[songIndex].song_name
                        self.lblArtistName.text = arrAllSong[songIndex].song_artist
                    }else{
                        MusicControll().nextSong()
                        MusicControll().playSongs(songIndex)
                        self.setPlayPauseButton()
                        self.lblSongName.text = arrAllSong[songIndex].song_name
                        self.lblArtistName.text = arrAllSong[songIndex].song_artist
                    }
                }
            }
            self.setPlayPauseButton()
            self.lblSongName.text = arrAllSong[songIndex].song_name
            self.lblArtistName.text = arrAllSong[songIndex].song_artist
        }
        
    }
    func formatTimeFromSeconds(totalSeconds: Int) -> String
    {
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds / 60) % 60
        //        let hours = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    func audioPlayer(_ audioPlayer: STKAudioPlayer, didStartPlayingQueueItemId queueItemId: NSObject) {
        MusicControll().remoteControl()
        self.UpdateControll()
        
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, didFinishBufferingSourceWithQueueItemId queueItemId: NSObject) {
        UpdateControll()
        MusicControll().remoteControl()
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, stateChanged state: STKAudioPlayerState, previousState: STKAudioPlayerState) {
        UpdateControll()
        appdelegate.background_play()
        MusicControll().remoteControl()
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, didFinishPlayingQueueItemId queueItemId: NSObject, with stopReason: STKAudioPlayerStopReason, andProgress progress: Double, andDuration duration: Double) {
        //self.appDelegate.secondTimer.invalidate()
        UpdateControll()
        playPauseSelected = 0
        MusicControll().remoteControl()
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
}
