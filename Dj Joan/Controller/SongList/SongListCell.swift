//
//  SongListCell.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 03/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMobileAds
class SongListCell: UITableViewCell,GADBannerViewDelegate {
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var imgSong: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewGIF: NVActivityIndicatorView!
    @IBOutlet weak var viewSong: UIView!
    @IBOutlet weak var adsView: GADBannerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewGIF.type = .lineScalePulseOutRapid // add your type
        viewGIF.color = UIColor(named: "MyTealColor")! // add your color
        viewGIF.startAnimating()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func reload(_ cellReload:Bool)
       {
           if cellReload == true
           {
               ViewContent.alpha = 0.7
               ViewContent.backgroundColor = MyColors.Red
           }
           else
           {
               ViewContent.alpha = 1
               ViewContent.backgroundColor = UIColor.clear
           }
       }
}
