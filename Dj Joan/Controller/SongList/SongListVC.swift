//
//  SongListVC.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 03/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
import PopupDialog
import SwiftGifOrigin
import GoogleMobileAds
class SongListVC: AdsVC{
    
    @IBOutlet weak var tblSongList: UITableView!
    @IBOutlet weak var tblPlayList: UITableView!
    @IBOutlet var viewPlaylist: UIView!
    var CurrentPage = 1
    var LastPage = 0
    var menuID = 0
    var categoryID = 0
    var HTitle = ""
    var songID = 0
    var Names = ""
    var index = 0
    var count = 0
    var checkScreen = "playList"
    var arrSongList:[songListData] = []
    var arrPlayList:[PlayListData] = []
    @IBOutlet weak var imgHideShow: UIImageView!
    @IBOutlet weak var viewPlayer: FooterView!
    @IBOutlet weak var HightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: HeaderView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSongList.register(UINib(nibName:"SongListCell", bundle: nil), forCellReuseIdentifier:"SongListCell")
        tblPlayList.register(UINib(nibName:"PlayListCell", bundle: nil), forCellReuseIdentifier:"PlayListCell")
        self.viewPlaylist.isHidden = true
        viewHeader.lblTitle.text = HTitle
        tblSongList.delegate = self
        tblSongList.dataSource = self
        tblPlayList.delegate = self
        tblPlayList.dataSource = self
        WBGetSongList()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewPlayer.commonInit()
        if miniStatus == .hide{
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }else{
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(tblReload), name: NSNotification.Name(rawValue: "cellClick"), object: nil)
    }
    @objc func tblReload(){
        tblSongList.reloadData()
    }
    @IBAction func btnHideShowView(_ sender: UIButton) {
        if miniStatus == .hide{
            miniStatus = .show
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            
            imgHideShow.image = UIImage(named: "icn_Down")
        }else{
            miniStatus = .hide
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }
        //        viewPlayer.commonInit()
    }
    @IBAction func btnClosePlayList(_ sender: Any) {
        self.viewPlaylist.isHidden = true
    }
    
}

extension SongListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblSongList{
            return arrSongList.count
        }else{
            return arrPlayList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblSongList{
            let cell = tblSongList.dequeueReusableCell(withIdentifier: "SongListCell", for: indexPath) as! SongListCell
            if  arrSongList[indexPath.row].song_name == "ads"{
                if appdelegate.google_adsKey == ""{
                    cell.adsView.adUnitID = "ca-app-pub-4328731208314277/2632012147"
                }else{
                    cell.adsView.adUnitID = appdelegate.google_adsKey
                }
                
                cell.adsView.rootViewController = self
                cell.adsView.load(GADRequest())
                cell.adsView.delegate = self
                cell.adsView.isHidden = false
                cell.viewSong.isHidden = true
            }else{
                cell.adsView.isHidden = true
                cell.viewSong.isHidden = false
                cell.lblSongName.text = arrSongList[indexPath.row].song_name
                cell.lblArtistName.text = arrSongList[indexPath.row].song_artist
                
                let img = arrSongList[indexPath.row].song_image
                cell.imgSong.sd_setImage(with: NSURL(string: (img!))! as URL,placeholderImage:UIImage(named: "icn_PlaceHolder"))
                if arrAllSong.count != 0{
                    if arrAllSong[songIndex].song_id == arrSongList[indexPath.row].song_id {
                        cell.imgGif.isHidden = true
                        cell.viewGIF.isHidden = false
                    }else{
                        cell.viewGIF.isHidden = true
                        cell.imgGif.isHidden = false
                    }
                }else{
                    cell.viewGIF.isHidden = true
                    cell.imgGif.isHidden = false
                }
                
                
                cell.btnMore.tag = indexPath.row
                cell.btnMore.addTarget(self, action: #selector(btnMoreClick), for: .touchUpInside)
                
                cell.btnPlay.tag = indexPath.row
                cell.btnPlay.addTarget(self, action: #selector(btnPlaySongClick), for: .touchUpInside)
            }
            return cell
        }else{
            let cell = tblPlayList.dequeueReusableCell(withIdentifier: "PlayListCell", for: indexPath) as! PlayListCell
            cell.lblName.text = arrPlayList[indexPath.row].playlist_name
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrSongList.count - 1 == indexPath.row{
            if LastPage > CurrentPage{
                let pagecount = CurrentPage + 1
                CurrentPage = pagecount
                WBGetSongList()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblPlayList{
            self.WBAddSonginPlaylist(playlistID:arrPlayList[indexPath.row].playlist_id!)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblPlayList{
            return 40
        }else{
            return 60
        }
        
        
    }
    @objc func btnMoreClick(_ sender: UIButton){
        if CheckConnection(){
            
            songID = arrSongList[sender.tag].song_id!
            let message = "Add Songs to Playlist or Favorites!"
            let popup = PopupDialog(title: title, message: message)
            self.present(popup, animated: true, completion: nil)
            index = sender.tag
            var PTitle = ""
            if checkScreen == "playList"{
                PTitle = "Remove to Playlist"
            }else{
                PTitle = "Add to Playlist"
            }
            let ok = CancelButton(title: PTitle) {
                print("Call API for Add to Playlist")
                if self.checkScreen == "playList"{
                    self.WBAddSonginPlaylist(playlistID: self.categoryID)
                }else{
                    self.popupAddtoplaylist()
                }
                
                //  self.popupAddtoplaylist()
            }
            var Ftitle = ""
            if checkScreen == "fav"{
                Ftitle = "Remove to Favorites"
            }else{
                Ftitle = "Add to Favorites"
            }
            let Delete = CancelButton(title: Ftitle) {
                print("Call API for Add to Favorites")
                if self.checkScreen == "fav"{
                    
                    self.WBAddFavSong(SongID:self.arrSongList[sender.tag].song_id!, Index: sender.tag)
                }else{
                    if self.arrSongList[sender.tag].favourites_status!{
                        self.showMyAlert(myMessage: "Song already added in favourites.")
                    }else{
                        
                        self.WBAddFavSong(SongID:self.arrSongList[sender.tag].song_id!, Index: sender.tag)
                    }
                }
                
                
            }
            
            popup.addButtons([ok,Delete])
        }
    }
    func popupAddtoplaylist() {
        let message = "Create Playlist or Add to Existing!"
        let popup = PopupDialog(title: title, message: message)
        self.present(popup, animated: true, completion: nil)
        
        let ok = CancelButton(title: "Create Playlist") {
            self.popupForPlaylist()
        }
        let Delete = CancelButton(title: "Add to Existing Playlist") {
            self.arrPlayList.removeAll()
            self.WBGetAndCreatePlaylist(type:"PlaylistList",playListName:"")
            
        }
        popup.addButtons([ok,Delete])
    }
    func popupForPlaylist() {
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        
        // Create first button
        let buttonTwo = CancelButton(title: "Cancel", height: 60) {
        }
        
        // Create second button
        let buttonOne = DefaultButton(title: "Create", height: 60) {
            print(ratingVC.commentTextField.text!)
            self.Names = ratingVC.commentTextField.text!
            if self.Names != ""{
                self.WBGetAndCreatePlaylist(type:"PlaylistAdd",playListName:self.Names)
            }else{
                self.showMyAlert(myMessage: "Enter Playlist Name")
            }
            
        }
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    
    @objc func btnPlaySongClick(_ sender: UIButton){
        let indexPath:IndexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell:SongListCell = tblSongList.cellForRow(at: indexPath) as! SongListCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { (bool) in
            cell.reload(false)
            
            let vc = self.storyboard?.instantiateViewController(identifier: "PopUpPlayerVC")as? PopUpPlayerVC
            vc!.menuID = self.arrSongList[sender.tag].menu_id ?? 0
            vc!.songID = self.arrSongList[sender.tag].song_id ?? 0
            arrAllSong.removeAll()
            for item in self.arrSongList{
                arrAllSong.append(item)
            }
            songIndex = sender.tag
            playPauseSelected = 1
            MusicControll().playSongs(songIndex)
            self.tblSongList.reloadData()
            self.navigationController?.pushViewController(vc!, animated: true)
            
        })
    }
    
    
}
extension SongListVC{
    func WBGetSongList() {
        if CheckConnection(){
            var url = ""
            var parameter:[String:Any]
            parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
            ] as [String : Any]
            if checkScreen == "fav"{
                url = API.GetAndAddFav +  "FavouritesSongsList&limit=50&page=" + "\(CurrentPage)"
            }else if checkScreen == "playList"{
                
                parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
                              ,"playlist_id":categoryID] as [String : Any]
                url = API.PlayListSong + "PlaylistSongsList&limit=50&page=" + "\(CurrentPage)"
            }else{
                if categoryID == 0{
                    url = API.MenuCategory + "\(menuID)" + "&menu_type=Tracks&limit=50&page=" + "\(CurrentPage)"
                }else{
                    url =  API.CategorySongAndVideo + "\(menuID)" + "&category_id=" + "\(categoryID)" + "&category_for=Tracks&limit=50&page=" + "\(CurrentPage)"
                }
            }
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = songListData(dict: item)
                            let dict = ["song_name":"ads","song_artist":"","song_image":""]
                            self.count = self.count + 1
                            if self.count == 8{
                                self.count = 0
                                let footer1 = songListData(dict: dict)
                                self.arrSongList.append(footer1)
                            }
                            
                            self.arrSongList.append(footer)
                        }
                    }
                    
                    if self.arrSongList.count == 0 {
                        self.showMyAlert(myMessage: "No Data Found")
                    }
                    self.CurrentPage = getInteger(apiResponse["current_page"])
                    self.LastPage = getInteger(apiResponse["last_page"])
                    self.tblSongList.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    
    
    func WBGetAndCreatePlaylist(type:String,playListName:String) {
        if CheckConnection(){
            
            var url = ""
            var parameter:[String:Any]
            if type == "PlaylistAdd"{
                url = API.GetAndCreatPlaylist + "PlaylistAdd"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_name":playListName,"menu_id": arrSongList[index].menu_id!]
            }else{
                url = API.GetAndCreatPlaylist + "PlaylistList"
                parameter = ["token":MyDefaults.getString(forKey: .jwtToken)]
            }
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = PlayListData(dict: item)
                            
                            self.arrPlayList.append(footer)
                        }
                        if self.arrPlayList.count == 0 {
                            self.showMyAlert(myMessage: "No Data Found")
                        }
                        
                        self.viewPlaylist.isHidden = false
                        self.tblPlayList.reloadData()
                    }else{
                        
                        self.WBAddSonginPlaylist(playlistID:getInteger(dic!["playlist_id"]))
                    }
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    
    func WBAddSonginPlaylist(playlistID:Int) {
        if CheckConnection(){
            
            var url = ""
            if checkScreen == "playList"{
                url = API.PlayListSong + "RemovePlaylistSongs"
            }else{
                url = API.PlayListSong + "AddPlaylistSongs"
            }
            
            let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"playlist_id":playlistID,"song_id":arrSongList[index].song_id!,"menu_id": arrSongList[index].menu_id!] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    self.showMyAlert(myMessage: getString(dic!["message"]))
                    if self.checkScreen == "playList"{
                        self.arrSongList.remove(at: self.index)
                        self.tblSongList.reloadData()
                    }else{
                        self.viewPlaylist.isHidden = true
                    }
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    func WBAddFavSong(SongID:Int,Index:Int) {
        if CheckConnection(){
            
            
            let url = API.GetAndAddFav + "AddRemoveSong"
            let parameter = ["token":MyDefaults.getString(forKey: .jwtToken),"song_id":SongID,"menu_id": arrSongList[Index].menu_id!] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject] , headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    if self.checkScreen == "fav"{
                        self.showMyAlert(myMessage: getString(dic!["message"]))
                        self.arrSongList.remove(at: Index)
                        self.tblSongList.reloadData()
                    }else{
                        self.showMyAlert(myMessage: getString(dic!["message"]))
                        self.arrSongList[Index].favourites_status = true
                    }
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    
}
