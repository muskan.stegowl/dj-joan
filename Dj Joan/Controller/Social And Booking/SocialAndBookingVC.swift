//
//  SocialAndBookingVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 16/03/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class sponserCellSocial: UICollectionViewCell {
    @IBOutlet weak var imgSponser: UIImageView!
}
class SocialAndBookingVC: AdsVC , UITextFieldDelegate,UITextViewDelegate{
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var colSlider: UICollectionView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var HeaderView: HeaderView!
    @IBOutlet weak var pageControl: UIPageControl!
    var timerStop = Timer()
    var moveLeft:Bool = false
    var contentOffX:CGFloat = CGFloat()
    var cellSpacing: CGFloat = 0
    var lineSpacing: CGFloat = 0
    var arrSlider:[sponserData] = []
    var SocialDic = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        HeaderView.lblTitle.text = "Social And Booking"
        txtMsg.delegate = self
        txtMsg.text = "COMMENT"
        txtMsg.textColor = UIColor.lightGray
        txtNumber.delegate = self
        txtName.attributedPlaceholder = NSAttributedString(string: "Enter your name", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter email", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtNumber.attributedPlaceholder = NSAttributedString(string: "Enter phone", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        colSlider.delegate = self
        colSlider.dataSource = self
        self.colSlider.isPagingEnabled = true
        
        //        setCollectionViewLayout()
        callSocialApi()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = txtNumber.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        txtNumber.text = formattedNumber(number: newString)
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtMsg.text == "COMMENT" {
            txtMsg.text = nil
            txtMsg.textColor = UIColor.white
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtMsg.text.isEmpty {
            txtMsg.text = "COMMENT"
            txtMsg.textColor = UIColor.lightGray
        }
    }
    @IBAction func btnSubmit(_ sender: Any) {
        if validation(){
            CallContactApi()
        }
    }
    
    func validation() -> Bool
    {
        if txtName.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterName)
            return false
        }
        else if txtEmail.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterEmail)
            return false
        }
        else if validateEmailWithString(txtEmail.text! as NSString)
        {
            showMyAlert(myMessage: MessageStrings().pleaseEnterAValidEmail)
            return false
        }
        else if txtNumber.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterPhoneNumber)
            return false
        }
        else if (txtNumber.text?.count)! < 12
        {
            
            showMyAlert(myMessage:MessageStrings().pleaseEnterValidPhoneNumber)
            return false
        }
        else if txtMsg.text == ""
        {
            showMyAlert(myMessage:"Please enter your comment")
            return false
        }
        else if txtMsg.text == "COMMENT"
        {
            showMyAlert(myMessage:"Please enter your comment")
            return false
        }
        
        return true
    }
    
    
    @IBAction func btninsta(_ sender: Any) {
        GoTOWeb(url: getString(SocialDic["instagram_link"]), title: "Instagram")
    }
    @IBAction func btnFacebook(_ sender: Any) {
        GoTOWeb(url: getString(SocialDic["facebook_link"]), title: "Facebook")
    }
    @IBAction func btnTwitter(_ sender: Any) {
        GoTOWeb(url: getString(SocialDic["twitter_link"]), title: "Twitter")
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        GoTOWeb(url: getString(SocialDic["other_link"]), title: "Other")
    }
    func GoTOWeb(url:String,title:String){
        let vc = storyboard?.instantiateViewController(withIdentifier: "WebViewVC")as? WebViewVC
        vc?.url = url
        vc?.titleH = title
        navigationController?.pushViewController(vc!, animated:false)
    }
    
}
extension SocialAndBookingVC{
    func CallContactApi() {
        if CheckConnection(){
            let PhoneNo = txtNumber.text!.replacingOccurrences(of: "-", with: "")
            
            let url =  API.Booking
            
            let parameter = [
                "token":MyDefaults.getString(forKey: .jwtToken),
                "name" : txtName.text!,
                "email" : txtEmail.text!,
                "phone" : txtNumber.text!,
                "comment" : txtMsg.text!
            ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    let msg = getString(dic!["message"])
                    self.showMyAlert(myMessage: msg)
                    self.txtName.text = ""
                    self.txtEmail.text = ""
                    self.txtNumber.text = ""
                    self.txtMsg.text = ""
                    self.txtMsg.text = "COMMENT"
                    self.txtMsg.textColor = UIColor.lightGray
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
        }
    }
    func callSocialApi()
    {
        
        if CheckConnection(){
            let url = API.SocialMedia
            
            let parameter = [  "token":MyDefaults.getString(forKey: .jwtToken)
            ] as [String : Any]
            print(parameter)
            print(url)
            
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                self.WBGetSponserSlider()
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    if let data = dic!["data"]{
                        let arrData = data as! NSArray
                        self.SocialDic = arrData[0] as! [String : Any]
                    }
                    
                    
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
            
        }
    }
    func WBGetSponserSlider() {
        if CheckConnection(){
            let url = API.BannerSlider
            
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)] as [String : Any]
            
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = sponserData(dict: item)
                            self.arrSlider.append(footer)
                            
                        }
                    }
                    self.pageControl.numberOfPages = self.arrSlider.count
                    self.pageControl.currentPage = 0
                    self.timerImages()
                    self.colSlider.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
}

extension SocialAndBookingVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrSlider.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (colSlider.frame.width), height: (colSlider.frame.height))
    }
    
    
    func  collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sponserCellSocial", for: indexPath) as! sponserCellSocial
        
        let img = arrSlider[indexPath.row].image
        
        Cell.imgSponser.sd_setImage(with:URL(string:img!),placeholderImage: UIImage(named: "icn_Slider"))
        
        //        Cell.imgSponser.image = UIImage(named: "icn_Slider")
        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let link =  arrSlider[indexPath.row].link
        
        if let url = URL(string: link!) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    //                UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                showMyAlert(myMessage: "Can't open website.")
            }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        
        let pageWidth = colSlider.frame.size.width
        let currentPage = colSlider.contentOffset.x / pageWidth
        
        if fmodf(Float(currentPage), 1.0) != 0.0{
            pageControl.currentPage = Int(currentPage) + 1
        }else{
            pageControl.currentPage = Int(currentPage)
        }
    }
    func timerImages()
    {
        
        Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
    }
    @objc func scrollToNextCell()
    {
        let pageWidth = colSlider.frame.size.width
        
        let currentPage = colSlider.contentOffset.x / pageWidth
        
        let cellSize = CGSize(width: self.view.frame.width,height: self.view.frame.height)
        let contentOffset = colSlider.contentOffset
        
        if (colSlider.contentSize.width <= colSlider.contentOffset.x + cellSize.width)
        {
            moveLeft = true
        }
        
        if contentOffset.x == 0
        {
            moveLeft = false
        }
        
        if moveLeft == true
        {
            pageControl.currentPage = Int(currentPage) - 1
            contentOffX = contentOffset.x - cellSize.width
            
            colSlider.scrollRectToVisible(CGRect(x: contentOffX,y: contentOffset.y,width: cellSize.width,height: cellSize.height), animated: true);
        }
        else
        {
            pageControl.currentPage = Int(currentPage) + 1
            contentOffX = contentOffset.x + cellSize.width
            
            colSlider.scrollRectToVisible(CGRect(x: contentOffX,y: contentOffset.y,width: cellSize.width,height: cellSize.height), animated: true);
        }
    }
    
}
