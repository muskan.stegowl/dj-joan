//
//  MenuVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 10/04/21.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
class MenuVC: AdsVC {
    @IBOutlet weak var imgHideShow: UIImageView!
    @IBOutlet weak var viewPlayer: FooterView!
    @IBOutlet weak var HightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var tblMenu: UITableView!
    var arrMenu:[menuData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenu.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        tblMenu.register(UINib(nibName: "MenuLogoCell", bundle: nil), forCellReuseIdentifier: "MenuLogoCell")
        tblMenu.delegate = self
        tblMenu.dataSource = self
        let arr =  [["menu_name":"My Favorite Videos","menu_image":"icn_Fav","menu_type":"favvideos"],["menu_name":"My Profile","menu_image":"Menu 16","menu_type":"profile"],["menu_name":"Notifications","menu_type":"noti","menu_image":"Menu 12"],["menu_name":"Social Media","menu_type":"social","menu_image":"Menu 13"]]
        for i in arr{
            let footer = menuData(dict: i)
            self.arrMenu.append(footer)
        }
        if MyDefaults.getBool(forKey: .loginUser){
            let user = menuData(dict:["menu_name":"Logout","menu_image":"Menu 16","menu_type":"Logout"])
            self.arrMenu.append(user)
            
        }else{
            let user = menuData(dict:["menu_name":"Login","menu_image":"Menu 16","menu_type":"Login"])
            self.arrMenu.append(user)
        }
        //        self.tblMenu.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewPlayer.commonInit()
        if miniStatus == .hide{
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }else{
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
            
        }
    }
    @IBAction func btnHideShowView(_ sender: UIButton) {
        if miniStatus == .hide{
            miniStatus = .show
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            
            imgHideShow.image = UIImage(named: "icn_Down")
        }else{
            miniStatus = .hide
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }
    }
    func callLogout() {
        let url = API.Signout
        
        let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
        ] as [String : Any]
        print(parameter)
        print(url)
        
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                self.GoNext(screenName: "LoginVC")
                MyDefaults.signOut()
                let msg = getString(dic!["message"])
                self.showMyAlert(myMessage: msg)
                
            }
            
        }
        
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
    }
    
}
extension MenuVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? arrMenu.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tblMenu.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
            cell.lblName.text = arrMenu[indexPath.row].menu_name
            cell.imgMenu.image = UIImage(named: arrMenu[indexPath.row].menu_image)
            cell.imgMenu.tintColor = UIColor.white
            cell.topSeprator.isHidden = indexPath.row != 0
            cell.bottomSeprator.isHidden = true//indexPath.row != (arrMenu.count - 1)
            return cell
        case 1:
            let cell = tblMenu.dequeueReusableCell(withIdentifier: "MenuLogoCell", for: indexPath) as! MenuLogoCell
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 50 : 300
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            return
        }
        let indexPath:IndexPath = IndexPath.init(row: indexPath.row, section: 0)
        let cell:MenuCell = tblMenu.cellForRow(at: indexPath) as! MenuCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { (bool) in
            cell.reload(false)
            
            let type = self.arrMenu[indexPath.row].menu_type?.lowercased()
            if type == "favvideos".lowercased(){
                let vc = self.storyboard?.instantiateViewController(identifier: "VideoListVC")as? VideoListVC
                //                    vc?.menuID = self.arrMenu[indexPath.row].menu_id!
                vc?.HTitle = "My Favorite Videos"
                vc?.isFav = true
                self.navigationController?.pushViewController(vc!, animated: true)
            }else if type == "profile".lowercased(){
                if MyDefaults.getBool(forKey: .loginUser){
                    self.GoNext(screenName: "ProfileVC")
                }else{
                    self.showAlertWith_Cancel_CompletionChoice(title: "", message: "Please login to continue") {
                        self.GoNext(screenName: "LoginVC")
                    }
                }
            }else if type == "noti".lowercased(){
                self.GoNext(screenName: "NotificationVC")
            }else if type == "social".lowercased(){
                self.GoNext(screenName: "SocialAndBookingVC")
            }else if type == "Logout".lowercased(){
                self.showAlertWith_Cancel_CompletionChoice(title: "", message: "Are you sure you want to logout?") {
                    self.callLogout()
                }
            }else if type == "Login".lowercased(){
                self.GoNext(screenName: "LoginVC")
            }
        })
    }
    
}
