//
//  MenuCellTableViewCell.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 30/11/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var topSeprator: UILabel!
    @IBOutlet weak var bottomSeprator: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ViewContent: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        reload(false)
    }
    func reload(_ cellReload:Bool)
    {
        if cellReload == true
        {
            ViewContent.alpha = 0.7
            ViewContent.backgroundColor = MyColors.Red
        }
        else
        {
            ViewContent.alpha = 1
            ViewContent.backgroundColor = UIColor.clear
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
