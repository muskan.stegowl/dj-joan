//
//  SongAlbumsCell.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 07/04/21.
//

import UIKit

class SongAlbumsCell: UICollectionViewCell {
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var footerHeight: NSLayoutConstraint!
    @IBOutlet weak var viewPlay: RoundView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
