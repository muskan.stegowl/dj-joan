//
//  SongAlbumsVC.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 07/04/21.
//

import UIKit
import Alamofire
import SDWebImage
import SVProgressHUD
import SwiftyJSON
class SongAlbumsVC: AdsVC {
    @IBOutlet weak var imgHideShow: UIImageView!
    @IBOutlet weak var viewPlayer: FooterView!
    @IBOutlet weak var HightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: HeaderView!
    @IBOutlet weak var colAlbums: UICollectionView!
    var arrAlbums : [HomeSubcategory] = []
    var CurrentPage = 1
    var LastPage = 0
    var HTitle = ""
    var CatId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        colAlbums.delegate = self
        colAlbums.dataSource = self
        colAlbums.register(UINib(nibName: "SongAlbumsCell", bundle: nil), forCellWithReuseIdentifier:"SongAlbumsCell")
        viewHeader.lblTitle.text = HTitle
        WBGetSongAlbums()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewPlayer.commonInit()
        if miniStatus == .hide{
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }else{
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
            
        }
    }
    @IBAction func btnHideShowView(_ sender: UIButton) {
        if miniStatus == .hide{
            miniStatus = .show
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            
            imgHideShow.image = UIImage(named: "icn_Down")
        }else{
            miniStatus = .hide
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }
    }
    func WBGetSongAlbums() {
        if CheckConnection(){
            let url =  API.SongAlbums  + "\(CatId)" + "?limit=15&page=" + "\(CurrentPage)"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken),"device":"IOS"] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = HomeSubcategory(dict: item)
                            self.arrAlbums.append(footer)
                        }
                    }
                    if self.arrAlbums.count == 0 {
                        self.showMyAlert(myMessage: "No Data Found")
                    }
                    self.CurrentPage = getInteger(apiResponse["current_page"])
                    self.LastPage = getInteger(apiResponse["last_page"])
                    self.colAlbums.reloadData()
                }
                
            }
            
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
    
}
extension SongAlbumsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrAlbums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"SongAlbumsCell", for:indexPath) as! SongAlbumsCell
        cell.lblTitle.text = arrAlbums[indexPath.row].category_name
        cell.lblCategoryName.text = arrAlbums[indexPath.row].parent_category_name
        let img = arrAlbums[indexPath.row].category_image
        cell.imgPoster.sd_setImage(with:URL(string:img!),placeholderImage: UIImage(named: "icn_PlaceHolder"))
//        cell.imgPoster.image = UIImage(named: "icn_PlaceHolder")
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let NextVC = storyboard?.instantiateViewController(identifier: "SongListVC")as? SongListVC
        NextVC?.categoryID = arrAlbums[indexPath.row].category_id!
        NextVC?.menuID = arrAlbums[indexPath.row].menu_id!
        NextVC!.checkScreen = ""
        NextVC?.HTitle = arrAlbums[indexPath.row].category_name!
        navigationController?.pushViewController(NextVC!, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if arrAlbums.count - 1 == indexPath.item{
            if LastPage > CurrentPage{
                let pagecount = CurrentPage + 1
                CurrentPage = pagecount
                WBGetSongAlbums()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth / 3 ) - 20
        let height = width + 40
        return CGSize(width: width, height: height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    // Cell Margin
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
//    }
    
}
