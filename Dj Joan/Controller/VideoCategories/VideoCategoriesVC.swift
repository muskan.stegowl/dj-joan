//
//  VideoCategoriesVC.swift
//  Dj Kenny
//
//

import UIKit
import AVKit
import AVFoundation
import SVProgressHUD
import MediaPlayer
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage
class VideoCategoriesVC: AdsVC {
    @IBOutlet weak var imgHideShow: UIImageView!
    @IBOutlet weak var viewPlayer: FooterView!
    @IBOutlet weak var HightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: HeaderView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    @IBOutlet weak var colVideoCategories: UICollectionView!
    var cellSpacing: CGFloat = 0
    var lineSpacing: CGFloat = 0
    var CurrentPage = 1
    var LastPage = 0
    var menuID = 1
    var HTitle = ""
    var arrCategories:[CategoriesData] = []
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if HTitle == ""{
            heightHeader.constant = 42
        }else{
            viewHeader.lblTitle.text = HTitle
            heightHeader.constant = 72
        }
        colVideoCategories.register(UINib(nibName: "VCategoriesCell", bundle: nil), forCellWithReuseIdentifier:"VCategoriesCell")
        colVideoCategories.delegate = self
        colVideoCategories.dataSource = self
        setCollectionViewLayout()
        WBGetVideoCategories()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        viewPlayer.commonInit()
        if miniStatus == .hide{
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }else{
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
            
        }
    }
    fileprivate func setCollectionViewLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let inset: CGFloat = 10
        layout.sectionInset = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
        self.cellSpacing = 8
        self.lineSpacing = 10
        layout.minimumLineSpacing = self.lineSpacing
        layout.minimumInteritemSpacing = self.cellSpacing
        self.colVideoCategories.collectionViewLayout = layout
    }
    @IBAction func btnHideShowView(_ sender: UIButton) {
        if miniStatus == .hide{
            miniStatus = .show
            if currentPlayStatus == .none{
                HightMiniPlayer.constant = 30
            }else if currentPlayStatus == .video {
                HightMiniPlayer.constant = 105
            }else{
                HightMiniPlayer.constant = 160
            }
            imgHideShow.image = UIImage(named: "icn_Down")
        }else{
            miniStatus = .hide
            HightMiniPlayer.constant = 30
            imgHideShow.image = UIImage(named: "icn_Up")
        }
    }
}
extension VideoCategoriesVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colVideoCategories.dequeueReusableCell(withReuseIdentifier: "VCategoriesCell", for: indexPath) as? VCategoriesCell
        cell?.lblVideoName.text = arrCategories[indexPath.item].category_name
        cell!.imgVideo.layer.cornerRadius = 20
//        cell!.imgVideo.clipsToBounds = true
        cell!.ViewContent.layer.cornerRadius = 20
//        cell!.ViewContent.clipsToBounds = true
        let img = arrCategories[indexPath.row].category_image
        cell?.imgVideo.sd_setImage(with: NSURL(string: (img!))! as URL,placeholderImage:UIImage(named: "icn_VH"))
        cell?.btnCellClick.tag = indexPath.row
        cell!.btnCellClick.addTarget(self, action: #selector(btnCellClick), for: .touchUpInside)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategories.count
    }
    
   func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       if arrCategories.count - 1 == indexPath.item{
           if LastPage > CurrentPage{
               let pagecount = CurrentPage + 1
               CurrentPage = pagecount
              WBGetVideoCategories()
           }
       }
   }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var cellInRow: CGFloat
        cellInRow = 2
        let width: CGFloat = (collectionView.bounds.width/cellInRow) - ((cellInRow-1)*self.cellSpacing)
        
        var height:CGFloat
        
        height = width * 0.55
        
        return CGSize(width: width, height: height)
        
    }
    @objc func btnCellClick(_ sender: UIButton){
        let indexPath:IndexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell:VCategoriesCell = colVideoCategories.cellForItem(at: indexPath) as! VCategoriesCell
        UIView.animate(withDuration: 0.2, animations: {
            cell.reload(true)
        }, completion: { (bool) in
            cell.reload(false)
            let vc = self.storyboard?.instantiateViewController(identifier: "VideoListVC")as? VideoListVC
            
            vc?.HTitle = self.arrCategories[indexPath.item].category_name!
            vc?.menuID = self.arrCategories[indexPath.item].menu_id!
            vc?.categoryID = self.arrCategories[indexPath.item].category_id!
            self.navigationController?.pushViewController(vc!, animated: true)
           
        })
    }
}
extension VideoCategoriesVC{
    func WBGetVideoCategories() {
        if CheckConnection(){
            let url =  API.MenuCategory + "\(menuID)" + "&menu_type=VideosCategory&limit=50&page=" + "\(CurrentPage)"
            let parameter = [ "token":MyDefaults.getString(forKey: .jwtToken)
                ] as [String : Any]
            print(url)
            print(parameter)
            AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
                print(apiResponse)
                
                let dic = apiResponse as NSDictionary?
                if self.CheckResponse(dic: dic as! [String : Any]){
                    
                    if let data = apiResponse["data"] as? [[String : Any]] {
                        for item in data {
                            let footer = CategoriesData(dict: item)
                            self.arrCategories.append(footer)
                        }
                    }
                    if self.arrCategories.count == 0 {
                        self.showMyAlert(myMessage: "No Data Found")
                    }
                    self.CurrentPage = getInteger(apiResponse["current_page"])
                    self.LastPage = getInteger(apiResponse["last_page"])
                    self.colVideoCategories.reloadData()
                }
                
            }
                
            ){ (error) -> Void in
                SVProgressHUD.dismiss()
                print(error)
            }
            
        }
    }
}
