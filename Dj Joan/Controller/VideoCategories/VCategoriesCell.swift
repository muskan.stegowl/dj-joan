//
//  VCategoriesCell.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 09/12/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import UIKit

class VCategoriesCell: UICollectionViewCell {
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var lblVideoName: UILabel!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnCellClick: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        reload(false)
    }
    func reload(_ cellReload:Bool)
    {
        if cellReload == true
        {
            ViewContent.alpha = 0.7
            ViewContent.backgroundColor = MyColors.Red
        }
        else
        {
            ViewContent.alpha = 1
            ViewContent.backgroundColor = UIColor.clear
        }
    }
}
