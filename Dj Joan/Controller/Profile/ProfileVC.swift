//
//  ProfileVC.swift
//  Dj Kenny
//
//  Created by iOS TeamLead on 15/03/21.
//  Copyright © 2020 Stegowl. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class ProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate{
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnPass: UIButton!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    var imageUrl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        loaddesign()
        // webViewOpen = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkTheActiveVC"), object: nil)
        CallGetProfileApi()
    }
    
    func loaddesign(){
         txtName.attributedPlaceholder = NSAttributedString(string: "Enter name", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
         txtUserName.attributedPlaceholder = NSAttributedString(string: "Enter user name", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Enter phone number", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        txtName.tintColor = UIColor.white
        txtUserName.tintColor = UIColor.white
        txtPhoneNumber.tintColor = UIColor.white
        txtPhoneNumber.delegate = self
        btnSave.layer.cornerRadius = 15
        btnSave.clipsToBounds = true
        btnPass.layer.cornerRadius = 15
        btnPass.clipsToBounds = true
        view1.layer.cornerRadius = 15
        view1.clipsToBounds = true
        view2.layer.cornerRadius = 15
        view2.clipsToBounds = true
        view3.layer.cornerRadius = 15
        view3.clipsToBounds = true
        view4.layer.cornerRadius = 15
        view4.clipsToBounds = true
        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = txtPhoneNumber.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        txtPhoneNumber.text = formattedNumber(number: newString)
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    private func showAlert1() {
        
        let alert = UIAlertController(title: "Image Selection", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.allowsEditing = true
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       
        var image : UIImage!
             if let img = info[.editedImage] as? UIImage {
                 image = img
             } else if let img = info[.originalImage] as? UIImage {
                 image = img
             }
           dismiss(animated: true,completion: nil)
//        self.dismiss(animated: true) { [weak self] in
//
//            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            //Setting image to your image view
        self.imgProfile.image = image
        self.uploadImageInDatabase(image:image)
//        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnHomeClick(_ sender: Any) {
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "openMainPlayer"), object: nil, userInfo: nil)
    }
    @IBAction func btnChangePasswordClick(_ sender: Any) {
     GoNext(screenName: "ChangePasswordVC")
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        GoBack()
    }
    @IBAction func btnLiveVideo(_ sender: Any) {
        //        appDelegate.videoBack = 3
        //        let obj = self.storyboard?.instantiateViewController(withIdentifier: "StreamVideoVC") as! StreamVideoVC
        //        appDelegate.objNewController = obj
        //
        //        NotificationCenter.default.post(name: Notification.Name(rawValue: "changeContainer"), object: nil, userInfo: ["vcIs" : obj])
    }
    
    @IBAction func btnEditeProfileClick(_ sender: Any) {
        showAlert1()
    }
    @IBAction func btnSaveClick(_ sender: Any) {
        if validation(){
            CallUpdateProfileApi()
        }
    }
    func validation() -> Bool
    {
        if txtName.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterName)
            
            return false
        }
        else if txtUserName.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterUsername)
            
            return false
        }
        else if txtPhoneNumber.text == ""
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterPhoneNumber)
            
            return false
        }
        else if (txtPhoneNumber.text?.count)! < 10
        {
            showMyAlert(myMessage:MessageStrings().pleaseEnterValidPhoneNumber)
            return false
        }
        
        return true
    }
}
extension ProfileVC{
    func CallGetProfileApi() {
        
        
        let url = API.ProfileDetails
        
        let parameter = [
            "token":MyDefaults.getString(forKey: .jwtToken)
            ] as [String : Any]
        print(parameter)
        print(url)
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                
                if let data = apiResponse["data"]{
                    let dic1 = data as? NSArray
                    let dic = dic1![0]as? NSDictionary
                    self.txtName.text = getString(dic?["name"])
                    self.txtUserName.text = getString(dic?["username"])
                    //
                    let aa = "\(getInteger(dic?["phone"]))"
                    
                    self.txtPhoneNumber.text = getString(dic?["phone"])
//                        formattedNumber(number: aa)
                    
                    
                    self.lblEmail.text = getString(dic?["email"])
                    self.imageUrl = getString(dic?["image"])
                    self.imgProfile.sd_setImage(with: URL(string: self.imageUrl), placeholderImage: UIImage(named: "icn_PlaceHolder"))
                }
                
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
        
    }
    
    func CallUpdateProfileApi() {
        
        let PhoneNo = txtPhoneNumber.text!.replacingOccurrences(of: "-", with: "")
        
        let url =  API.ProfileUpdate
        
        let parameter = [
            "token" : MyDefaults.getString(forKey: .jwtToken),
            "name" :txtName.text!,
            "username" :txtUserName.text!,
            "email" :lblEmail.text!,
            "phone" :txtPhoneNumber.text!,
            "image" : imageUrl
            
            ] as [String : Any]
        
        SVProgressHUD.show()
        print(url)
        print(parameter)
        AFWrapper.requestPOSTURL(url,showLoader : true,enableJWT : false, params: parameter as [String : AnyObject], headers: nil, success: { (apiResponse) in
            print(apiResponse)
            
            let dic = apiResponse as NSDictionary?
            if self.CheckResponse(dic: dic as! [String : Any]){
                let msg = getString(dic!["message"])
                self.showMyAlert(myMessage: msg)
                self.CallGetProfileApi()
            }
            
        }
            
        ){ (error) -> Void in
            SVProgressHUD.dismiss()
            print(error)
        }
        
        
        
    }
    func uploadImageInDatabase(image:UIImage)
    {
        
        let imageData = image.jpegData(compressionQuality: 0.5)
        let url = API.UploadImage
        print(url)
        let date = NSDate()
        let df = DateFormatter()
        df.dateFormat = "dd-mm-yy-hh-mm-ss"
        
        let imageName = df.string(from: date as Date)
        print(imageName)
        
        SVProgressHUD.show(withStatus: "Loading")
        AF.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(imageData!, withName: "image", fileName: "\(imageName).jpeg", mimeType: "image/jpeg")
        },
                  to: url, method: .post , headers:nil).response { resp in
                    switch resp.result {
                    case .success(let value):
                        if let data = value {
                            let resJson = JSON(data)
                            let dic1 = JSON(resJson)
                            print(dic1)
                            let status = resJson["status"].number
                            if status == 200 {
                                let dic = (dic1.dictionaryObject)as NSDictionary?
                                self.imageUrl = getString(dic!["url"])
                                
                                
                                print(self.imageUrl)
                                SVProgressHUD.dismiss()
                                self.showMyAlert(myMessage:(getString(dic!["message"]) as NSString) as String)
                                
                            }else{
                                print(data)
                                SVProgressHUD.dismiss()
                            }
                        }
                        
                    case .failure(let error):
                        SVProgressHUD.dismiss()
                        print(error)
                    }
                    
        }
        
        
    }
}
