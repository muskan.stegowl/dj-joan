//
//  WebService.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 28/11/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import Foundation
fileprivate var baseURL: String{
//     return "http://107.22.139.221/newdj/api/"
//    return "http://34.207.157.107/djjoan/api/"
    return "https://djjoanapp.com/api/"
}

enum API {
    static let GoogleAds = baseURL + "googleads"
    static let login = baseURL + "signin"
    static let register = baseURL + "signup"
    static let ForgotPassword = baseURL + "forgotpassword"
    static let SkipUser = baseURL + "skipuser"
    static let Menu = baseURL + "menu"
    static let Notifications = baseURL + "notifications?limit=50&page="
    static let LegalDetails = baseURL + "legaldetails"
    static let Gallery = baseURL + "gallery?limit=50&page="
    static let ProfileUpdate = baseURL + "profile?type=ProfileUpdate"
    static let ProfileDetails = baseURL + "profile?type=ProfileDetails"
    static let Signout = baseURL + "signout"
    static let ChangePassword = baseURL + "changepassword"
    static let SocialMedia = baseURL + "socialmedia"
    static let Booking = baseURL + "booking"
    static let ShareMyApp = baseURL + "sharemyapp"
    static let UploadImage = baseURL + "uploads"
    static let MenuCategory = baseURL + "menuitems?menu_id="
    static let CategorySongAndVideo = baseURL + "categoryitems?menu_id="
    static let LikesDisLikeSong = baseURL + "likes"
    static let Biography = baseURL + "biography"
    static let InstagramLink = baseURL + "instagramlink"
    static let Subscriptions = baseURL + "subscriptions"
    static let GetAndCreatPlaylist = baseURL + "playlist?type="
    static let PlayListSong = baseURL + "playlistsongs?type="
    static let GetAndAddFav = baseURL + "favourites?type="
    static let Search = baseURL + "search?limit=50&page="
    static let DjDrakeLink = baseURL + "djdrakelink"
    static let Support = baseURL + "support"
    static let SongAndVideoDetails = baseURL + "categoryitemdetails?type="
    static let Front = baseURL + "homeslider"
    static let FavVideo = baseURL + "favouritevideos?type="
    static let SponsorSlider = baseURL + "sponsorslider"
    static let VideoLikeDisLike = baseURL + "videolikedislike"
    static let TotalShared = baseURL + "totalshared"
    static let PlayCount = baseURL + "playcount"
    static let MenuStatus = baseURL + "menustatus"
    static let HomeItems = baseURL + "homeitems"
    static let Home = baseURL + "home?limit=50&page="
    static let SongAlbums = baseURL + "audiosubcategories/"
    static let BannerSlider = baseURL + "bannerslider"
    static let IntroSlider = baseURL + "walkthrough"
    
}
