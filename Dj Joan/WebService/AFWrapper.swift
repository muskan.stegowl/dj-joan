//
//  AFWrapper.swift
//  iBabelClient
//
//  Created by Raxit on 07/10/16.
//  Copyright © 2016 Crest-Infotech. All rights reserved.
//
//AFWrapper
import UIKit
import Alamofire
import SwiftyJSON
import Reachability
import SVProgressHUD
class AFWrapper: NSObject {
    class func requestGETURL(_ strURL: String,showLoader: Bool = true, enableJWT:Bool, headers : [String : String]?, success:@escaping (_ data: [String : Any]) -> Void,failure:@escaping (Error) -> Void) {
        if isDeviceConnectedToInternet() {
            showLoader ? SVProgressHUD.show() : ()
            AF.request(strURL, method: .get, encoding: JSONEncoding.default, headers:nil).responseJSON { (response) -> Void in
                showLoader ? SVProgressHUD.dismiss() : ()
                switch response.result {
                case .success(let value):
                  
                        if let json = value as? [String: Any] {
                            
                            let resJson = JSON(json)
                            let dic1 = JSON(resJson)
                            let dic = (dic1.dictionaryObject)as NSDictionary?
                            success(dic as! [String : Any])
                            
                        }
                    
                    
                case .failure(let error):
                    print(error)
                    failure(error)
                }
            }
        }else{
            if let vc = UIApplication.topViewController(){
                vc.showMyAlert(myMessage:MessageStrings().NoInternet)
            }
        }
    }
    class func requestPOSTURL(_ strURL : String ,showLoader:Bool, enableJWT:Bool, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (_ data: [String : Any]) -> Void,failure:@escaping (Error) -> Void){
        if isDeviceConnectedToInternet() {
    
            showLoader ? SVProgressHUD.show() : ()
            AF.request(strURL, method: .post,parameters: params, encoding: JSONEncoding.default, headers:nil).responseJSON { (response) -> Void in
                showLoader ? SVProgressHUD.dismiss() : ()
                switch response.result {
                case .success(let value):
                   
                        if let json = value as? [String: Any] {
                            
                            let resJson = JSON(json)
                            let dic1 = JSON(resJson)
                            let dic = (dic1.dictionaryObject)as NSDictionary?
                            success(dic as! [String : Any])
                            
                         }
                    
                case .failure(let error):
                    print(error)
                    failure(error)
                }
            }
        }else{
            if let vc = UIApplication.topViewController(){
                vc.showMyAlert(myMessage:MessageStrings().NoInternet)
            }
        }
        
    }
    
}

