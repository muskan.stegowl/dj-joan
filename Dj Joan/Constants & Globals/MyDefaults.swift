
import UIKit




enum MyDefaults: String {
    
    //get
   // MyDefaults.getString(forKey: .user_id)
    
    //set
//    MyDefaults.set(userId, forKey: .user_id)
    
    //remove
    
//      UserDefaults.standard.removeObject(forKey: MyDefaults.email.rawValue)
    
 
      static var keys: [MyDefaults] = [.fcmToken, .user_id, .name, .mobile, .email,.jwtToken,.loginUser]
    
    case name = "name" // string
    case email = "email" // string
    case mobile = "mobile" // string
    case loginUser = "loginUser"
    case fcmToken = "fcmToken" // string
    case user_id = "user_id" // string
    case jwtToken = "jwtToken" // string
    case isSkip = "isSkip" // Bool
    
  

    
    /// Saves 'Any' Value for the given key
    static func set(_ value: Any?, forKey key: MyDefaults) {
        //        if let val = value {
        //            UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.setValue(value, forKey: key.rawValue)
        //        }
    }
   

    /// Returns Value as 'Any' for the given key
    static func getValue(forKey key: MyDefaults) -> Any? {
        return UserDefaults.standard.value(forKey: key.rawValue)
    }
    
    /// Returns the 'String' value of given key
    static func getString(forKey key: MyDefaults) -> String {
        return Dj_Joan.getString(UserDefaults.standard.value(forKey: key.rawValue))
    }
    
    /// Returns the 'Integer' value of given key
    static func getInteger(forKey key: MyDefaults) -> Int {
        return Dj_Joan.getInteger(UserDefaults.standard.value(forKey: key.rawValue))
    }
    
    /// Returns the 'Float' value of given key
    static func getFloat(forKey key: MyDefaults) -> Float {
        return Dj_Joan.getFloat(UserDefaults.standard.value(forKey: key.rawValue))
    }
    
    /// Returns the 'Double' value of given key
    static func getDouble(forKey key: MyDefaults) -> Double {
        return Dj_Joan.getDouble(UserDefaults.standard.value(forKey: key.rawValue))
    }
    
    /// Returns the 'Bool' value of given key
    static func getBool(forKey key: MyDefaults) -> Bool {
        return Dj_Joan.getBool(UserDefaults.standard.value(forKey: key.rawValue))
    }
    
}


extension MyDefaults : CaseIterable {
    
    /// Returns the dictionary of all Key-Values as [Key : Value] format
    static func getAllDefaults() -> [String: Any?] {
        var result = [String: Any?]()
        
        for key in MyDefaults.allCases {
            let val = getValue(forKey: key)
            result[key.rawValue] = val
        }
        
        print("\(result.count) Values Found!")
        return result
    }
    
    /// Removes All Key-Values From UserDefaults
    static func removeall() {
        MyDefaults.allCases.forEach { (key) in
            UserDefaults.standard.removeObject(forKey: key.rawValue)
        }
    }
    
    /// Removes the Key-Values that are used while user is logged in.
    static func signOut() {
    // Leave the keys that are required to manage the App Status like 'selectedLanguage'
        MyDefaults.allCases.forEach { (key) in
//            if (key != .languageName) && (key != .languageCode) {
//            if (key != .cachedEmails) {
            
           // MyControllers.goToLogin()
            UserDefaults.standard.removeObject(forKey: key.rawValue)
//            }
        }
        self.removeall()
    }
    
}




