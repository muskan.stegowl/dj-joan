//
//  HeaderView.swift
//  Dj kenny
//
//  Created by Stegowl's Macbook on 26/12/20.
//  Copyright © 2020 Stegowl's Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class HeaderView: UIView{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var backHeight: NSLayoutConstraint!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgLiveTV: UIImageView!
    @IBOutlet weak var imgLiveRadio: UIImageView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var imgPlayList: UIImageView!
    @IBOutlet weak var imgMenu: UIImageView!
    let StatepickerView = UIPickerView()
    let nibName = "HeaderView"
    var contentView:UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder){
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        if let vc = UIApplication.topViewController(){
            if vc as? VideoCategoriesVC != nil{
                if catViewAll{
                    backHeight.constant = 30
                }else{
                    backHeight.constant = 0
                }
            }else if vc as? RadioVC != nil{
                backHeight.constant = 0
            }else if vc as? FavouritSongAndPlayListVC != nil{
                backHeight.constant = 0
            }else if vc as? HomeVC != nil{
                backHeight.constant = 0
            }else if vc as? MenuVC != nil{
                backHeight.constant = 0
            }else if "\(vc)".components(separatedBy: ":")[0] == "<GADFullScreenAdViewController"{
                backHeight.constant = 0
            }else{
                backHeight.constant = 30
            }
            
        }
        SetMenu()
    }
    
    func SetMenu(){
        if currentMenu == .home{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor(named: "MyTealColor")
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor.white
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor.white
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor.white
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor.white
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor.white
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor.white
        }else if currentMenu == .liveTV{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor.white
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor(named: "MyTealColor")
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor.white
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor.white
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor.white
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor.white
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor.white
            
        }else if currentMenu == .liveRadio{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor.white
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor.white
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor(named: "MyTealColor")
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor.white
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor.white
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor.white
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor.white
            
        }else if currentMenu == .video{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor.white
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor.white
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor.white
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor(named: "MyTealColor")
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor.white
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor.white
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor.white
        }else if currentMenu == .fav{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor.white
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor.white
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor.white
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor.white
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor(named: "MyTealColor")
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor.white
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor.white
        }else if currentMenu == .playList{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor.white
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor.white
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor.white
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor.white
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor.white
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor(named: "MyTealColor")
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor.white
        }else{
            imgHome.image = UIImage.init(systemName: "music.note.list")
            imgHome.tintColor = UIColor.white
            
            imgLiveTV.image = UIImage(named: "icn_LiveTV")
            imgLiveTV.tintColor = UIColor.white
            
            imgLiveRadio.image = UIImage(named: "icn_Radio")
            imgLiveRadio.tintColor = UIColor.white
            
            imgVideo.image = UIImage(named: "icn_VideoListing")
            imgVideo.tintColor = UIColor.white
            
            imgFav.image = UIImage(named: "icn_AddToFav")
            imgFav.tintColor = UIColor.white
            
            imgPlayList.image = UIImage(named: "icn_AddToPlayList")
            imgPlayList.tintColor = UIColor.white
            
            imgMenu.image = UIImage(named: "icn_Menu")
            imgMenu.tintColor = UIColor(named: "MyTealColor")
            
        }
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            if currentVC as? VideoListVC != nil{
                currentVC.GoBack()
                appdelegate.background_play()
                appdelegate.configurePlayer()
            }else{
                
                currentVC.GoBack()
            }
           
            
        }
    }
    @IBAction func btnHome(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            currentMenu = .home
            currentVC.GoNext(screenName: "HomeVC")
        }
    }
    @IBAction func btnLiveVideo(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
//            currentMenu = .liveTV
            currentVC.GoNext(screenName: "LiveVideoVC")
        }
    }
    @IBAction func btnLiveRadio(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            currentMenu = .liveRadio
            currentVC.GoNext(screenName: "RadioVC")
        }
    }
    @IBAction func btnVideoCollection(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            currentMenu = .video
            catViewAll = false
            currentVC.GoNext(screenName: "VideoCategoriesVC")
        }
    }
    @IBAction func btnFav(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            currentMenu = .fav
            let vc = currentVC.storyboard?.instantiateViewController(identifier: "FavouritSongAndPlayListVC") as? FavouritSongAndPlayListVC
            vc?.isPlayList = false
            currentVC.navigationController?.pushViewController(vc!, animated: false)
            
        }
    }
    @IBAction func btnPlayList(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            currentMenu = .playList
            let vc = currentVC.storyboard?.instantiateViewController(identifier: "FavouritSongAndPlayListVC") as? FavouritSongAndPlayListVC
            vc?.isPlayList = true
            currentVC.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    @IBAction func btnMenu(_ sender: UIButton) {
        if let currentVC = UIApplication.topViewController() {
            currentMenu = .menu
            currentVC.GoNext(screenName: "MenuVC")
        }
    }
}
