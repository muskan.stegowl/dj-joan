//
//  StringsConstant.swift
//  Copyright © 2020 flowactivo. All rights reserved.
//

import Foundation

class MessageStrings  {
    var passwordAndConfirmPasswordNotMatch = "Password and confirm password doesn't match"
    var error = "Error"
    var takePhoto = "Take Photo"
    var cameraRoll = "Camera Roll"
    var photoLibrary = "Photo Library"
    var cancel = "cancel"
    var success = "Success"
    var somethingGoodHappened = "something Good Happened"
    var retry = "retry"
    var warning = "warning"
    var somethingWentWrongTryAfterSometimes = "This song is temporary not available please try again later."
    var infomation = "infomation"
    var pleaseCheckYourInternetConnection = "Please Check Your Internet Connection"
    var pleaseEnterEmail = "Please Enter Email"
    var pleaseEnterAValidEmail = "Please Enter A Valid Email"
    var pleaseEnterAValidEmailorPhone = "Please Enter A Valid Email or Mobile No."
    var pleaseEnterAValidPhone = "Please Enter A Mobile No."
    var pleaseEnterYourFullName = "please Enter Your Full Name"
    var pleaseEnterPassword = "Please enter your password"
    var pleaseEnterConfirmPassword = "Please enter your confirm password"
    var pleaseEnterValidPassword = "The password must contain at least 6 or more characters."
    var ConfirmdPasswordNotMatch = "New Password and Confirm Password must be the same"
    var more = "more"
    var less = "less"
    var pleaseCheckYourEmail = "Please Check your email id"
    var forgotPasswordLinkSendSuccessfully = "Forgot Password Link Send Successfully"
    var conciergeRise = "Concierge Request Send Successfully"
    var noblank = "Textfield can't be blank"
    var pleaseEnterEmailorUsername = "Enter Username -or- Enter Email"
    var pleaseEnterUsername = "Please enter your Username"
    var pleaseEnterName = "Please enter your Name"
    var pleaseEnterPhoneNumber = "Please enter your Phone Number"
    var pleaseEnterValidPhoneNumber = "Please enter a valid Mobile Number"
    var pleaseEnterDOB = "Please enter your DOB"
    var noData = "No Data Found"
    var  NoInternet = "No Internet Connection"

    //
}
