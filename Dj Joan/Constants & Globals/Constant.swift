//
//  Constant.swift
//  Dj Drake
//
//  Created by Stegowl Macbook Pro on 28/11/20.
//  Copyright © 2020 Stegowl Macbook Pro. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation
import StreamingKit
@available(iOS 13.0, *)
@available(iOS 13.0, *)
var appdelegate: AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}
var InstaLink = ""
var isPlaySong = false
var isFirstPlay = false
var isVideo = false
var isVideoPlay = false
var isRadio = false
var isLastPlay = ""
var globalShuffle = 0
var globalRepeat = 0
var isMute = false
var songIndex = 0
var playPauseSelected = 0
var secondTimer = Timer()
var isLoginUser:Bool = false
var arrAllSong = [songListData]()
var arrLiveRadio = [LiveRadioData]()
var arrAllVideo = [VideoListData]()
var liveVideoMenusStatus = false
var radioMenusStatus = false
var liveVideoMenusID = 0
var radioMenusID = 0
var videoIndex = 0
var miniStatus:MiniPlayerStatus = .hide
var currentPlayStatus:CurrentPlayStatus = .none
var currentMenu:CurrentMenu = .home
var currentRadioIndex = 0
var catViewAll = false
var FcmID = "cgyH0KdZdknhswyCQNvfk4:APA91bFVtv2GhpcuE-bJhujM6fiZbHdSJ4f1gc-6aNp40sA7NTb2b0AQXxnFVP1PnoKau6YzQMW8FT12EBgbt3BTl5OGFH3LxeBHsKySJxYZIpSZHhHXN55S3TmRDz5j5a1aSgjit6hQ"

var isV = false
enum MiniPlayerStatus{
    case show
    case hide
}
enum CurrentPlayStatus{
    case video
    case song
    case none
}
enum CurrentMenu{
    case home
    case liveTV
    case liveRadio
    case video
    case fav
    case playList
    case menu
        
}
extension UIViewController{
    func GoNext(screenName: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: screenName)
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GoBack() {
        self.navigationController?.popViewController(animated: false)
    }
}


extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

func validateEmailWithString(_ Email: NSString) -> Bool {
    //    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return !emailTest.evaluate(with: Email)
}
func isEmptyString(_ text : NSString) -> Bool
{
    if text .isEqual(to: "") || text.trimmingCharacters(in: CharacterSet.whitespaces) .isEmpty
    {
        return true
    }
    else
    {
        return false
    }
    
}
func checkValueIntString(Value:String) -> Bool{
    let number = Value
    let numberCharacters = NSCharacterSet.decimalDigits.inverted
    if !number.isEmpty && number.rangeOfCharacter(from: numberCharacters) == nil {
        print("string is a valid number")
        return true
    } else {
        print("string contained non-digit characters")
        return false
    }
}

func checkType(_ value: Any) {
    switch value {
    case is String:
        print("String")
    //                break
    case is Int:
        print("Int")
    //                break
    case is Float:
        print("Float")
    //                break
    case is Double:
        print("Double")
    //                break
    case is Bool:
        print("Bool")
    //                break
    case is NSDictionary:
        print("NSDictionary")
    //                break
    case is NSArray:
        print("NSArray")
    //                break
    default:
        print("Other")
        //                break
    }
}


/// Returns Integer Value or 0
func getInteger(_ obj: Any?) -> Int {
    if let num = obj as? NSNumber {
        return num.intValue
    }
    else if let str = obj as? NSString {
        return str.integerValue
    }
    else {
        print("NEITHER A STRING NOR A NUMBER")
    }
    return 0
}


/// Returns Integer Value or 0
func getFloat(_ obj: Any?) -> Float {
    if let num = obj as? NSNumber {
        return num.floatValue
    }
    else if let str = obj as? NSString {
        return str.floatValue
    }
    else {
        print("NEITHER A STRING NOR A NUMBER")
    }
    return 0
}

func getDouble(_ obj: Any?) -> Double {
    if let num = obj as? NSNumber {
        return num.doubleValue
    }
    else if let str = obj as? NSString {
        return str.doubleValue
    }
    else {
        print("NEITHER A STRING NOR A NUMBER")
    }
    return 0
}


func getString(_ obj: Any?) -> String {
    if let str = obj as? NSString {
        return str as String
    }
    else if let num = obj as? NSNumber {
        return num.stringValue
    }
    else {
        print("NEITHER A STRING NOR A NUMBER")
    }
    return ""
}


func getBool(_ obj: Any?) -> Bool {
    if let bool = obj as? Bool {
        return bool
    } else if let str = obj as? String {
        return (str.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() == "yes") ? true : false
    } else {
        return false
    }
    
}
@IBDesignable class RoundButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        //        refreshCR(_value: cornerRadius)
        //        refreshBorder(_color: customBGColor)
        refreshBorderColor(_colorBorder: customBorderColor)
        refreshBorder(_borderWidth: borderWidth)
        self.tintColor = UIColor.white
        refreshCorners(value: cornerRadius)
    }
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            refreshBorder(_borderWidth: borderWidth)
        }
    }
    
    func refreshBorder(_borderWidth: CGFloat) {
        layer.borderWidth = _borderWidth
    }
    
    @IBInspectable var customBorderColor: UIColor = UIColor.init (red: 0, green: 122/255, blue: 255/255, alpha: 1){
        didSet {
            refreshBorderColor(_colorBorder: customBorderColor)
        }
    }
    
    func refreshBorderColor(_colorBorder: UIColor) {
        layer.borderColor = _colorBorder.cgColor
    }
    
    
}
@IBDesignable class RoundView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        //        refreshCR(_value: cornerRadius)
        //        refreshBorder(_color: customBGColor)
        refreshBorderColor(_colorBorder: customBorderColor)
        refreshBorder(_borderWidth: borderWidth)
        self.tintColor = UIColor.white
        refreshCorners(value: cornerRadius)
    }
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            refreshBorder(_borderWidth: borderWidth)
        }
    }
    
    func refreshBorder(_borderWidth: CGFloat) {
        layer.borderWidth = _borderWidth
    }
    
    @IBInspectable var customBorderColor: UIColor = UIColor.init (red: 0, green: 122/255, blue: 255/255, alpha: 1){
        didSet {
            refreshBorderColor(_colorBorder: customBorderColor)
        }
    }
    
    func refreshBorderColor(_colorBorder: UIColor) {
        layer.borderColor = _colorBorder.cgColor
    }
    
    
}
@IBDesignable class RoundImage: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        //        refreshCR(_value: cornerRadius)
        //        refreshBorder(_color: customBGColor)
        refreshBorderColor(_colorBorder: customBorderColor)
        refreshBorder(_borderWidth: borderWidth)
        self.tintColor = UIColor.white
        refreshCorners(value: cornerRadius)
    }
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            refreshBorder(_borderWidth: borderWidth)
        }
    }
    
    func refreshBorder(_borderWidth: CGFloat) {
        layer.borderWidth = _borderWidth
    }
    
    @IBInspectable var customBorderColor: UIColor = UIColor.init (red: 0, green: 122/255, blue: 255/255, alpha: 1){
        didSet {
            refreshBorderColor(_colorBorder: customBorderColor)
        }
    }
    
    func refreshBorderColor(_colorBorder: UIColor) {
        layer.borderColor = _colorBorder.cgColor
    }
    
    
}
extension UIViewController {
    func displayShareSheet(_ songName:String ,songUrl:String,artistName:String  ,appName:String)
    {
        let activityViewController = UIActivityViewController(activityItems:[songName,songUrl,artistName,appName], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    
    func showAlertWith_Cancel_CompletionChoice(title: String, message: String, completion: @escaping ()-> Void)
    {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //    alertController.view.tintColor = themeColor
        alertController.view.layer.cornerRadius = 10
        alertController.view.clipsToBounds = true
        
        
        let completionAction = UIAlertAction(title: "Yes", style: .default) { (_) in
            completion()
        }
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        //        let completionAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(completionAction)
        alertController.addAction(cancelAction)
        
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        
        //        completion()
    }
    
    func showMyAlert(myMessage : String) -> Void {
        let alertController = UIAlertController(title: "", message: myMessage, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            (result : UIAlertAction) -> Void in
            print("You pressed OK")
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func CheckConnection() -> Bool {
        
        if isDeviceConnectedToInternet() {
            print("Online Mode")
            return true
        }else{
            showMyAlert(myMessage:MessageStrings().NoInternet)
            return false
        }
    }
    
    func CheckResponse(dic:[String:Any]) -> Bool {
        let status = getInteger(dic["status"])
        let msg = getString(dic["message"])
        if status == 200{
            return true
        }else if status == 401{
            self.GoNext(screenName: "LoginVC")
            return false
        }else{
            showMyAlert(myMessage:msg)
            return false
        }
    }
    func CheckResponse1(dic:[String:Any]) -> Bool {
        let status = getInteger(dic["status"])
        let msg = getString(dic["message"])
        if status == 200{
            return true
        }else{
            showMyAlert(myMessage:msg)
            return false
        }
    }
}

func formattedNumber(number: String) -> String {
    let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    let mask = "XXX-XXX-XXXX"
    
    var result = ""
    var index = cleanPhoneNumber.startIndex
    for ch in mask where index < cleanPhoneNumber.endIndex {
        if ch == "X" {
            result.append(cleanPhoneNumber[index])
            index = cleanPhoneNumber.index(after: index)
        } else {
            result.append(ch)
        }
    }
    return result
}

enum MyColors {
    //----------------------------------------------------------------------------
    
    static var Red: UIColor {
        return UIColor(named: "MyTealColor")!
    }
    static var Black: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
    }
}

class CandyGradientView: UIView {
    
    var appSymbol: UIImageView?
    var pinkGradient: CAGradientLayer?
    
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    //    override init(frame: CGRect) {
    //        super.init(frame: frame)
    //        self.loadDesign()
    //    }
    //
    //    required init?(coder aDecoder: NSCoder) {
    //        super.init(coder: aDecoder)
    //        self.loadDesign()
    //    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.colors = [UIColor.clear, MyColors.Red.cgColor]
        //         gradientLayer.colors = [MyColors.pinkForGradient.cgColor, MyColors.blueForGradient.cgColor]
        gradientLayer.locations = [0.0, 1.0]
    }
    
    private func loadDesign() {
        self.clipsToBounds = true
        //        self.backgroundColor = .white
        
        DispatchQueue.main.async {
            self.addAppSymbol()
            //            self.addPinkGradient()
        }
    }
    
    private func addAppSymbol() {
        self.appSymbol = UIImageView(frame: .zero)
        self.addSubview(self.appSymbol!)
//        self.addConstraintsToImage(targetView: self.appSymbol!, parentView: self)
        //self.appSymbol?.image = MyImages.appSymbol
        self.appSymbol?.contentMode = .scaleAspectFit
    }
    
    private func addPinkGradient() {
        //        self.pinkGradient = UIView.gradientLayer(locations: [0.0, 0.6, 1.0], colors: [MyColors.blueForGradient, MyColors.pinkForGradient, MyColors.pinkForGradient])
        self.pinkGradient = UIView.gradientLayer(locations: [0.0, 1.0], colors: [UIColor.clear, MyColors.Red])
        self.pinkGradient?.frame = self.bounds
        self.layer.addSublayer(self.pinkGradient!)
        
        if let appSymbol = self.appSymbol {
            self.bringSubviewToFront(appSymbol)
        }
        
    }
    
    
    func addConstraintsToImage(targetView: UIView, parentView: Any) {
        //        let targetView = UIView()
        //        Align centre x to superview
        //        Align centre y to superview
        //        Proportional width to superview 1.7
        //        700:630 aspect ratio
        //        Topspace to sueprview >= 0
        //        Proportional height to superview 0.9
        targetView.translatesAutoresizingMaskIntoConstraints = false
        let centerX = NSLayoutConstraint(item: targetView, attribute: .centerX, relatedBy: .equal, toItem: parentView, attribute: .centerX, multiplier: 1.0, constant: 0)
        
        let centerY = NSLayoutConstraint(item: targetView, attribute: .centerY, relatedBy: .equal, toItem: parentView, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        let proportionalWidth = NSLayoutConstraint(item: targetView, attribute: .width, relatedBy: .equal, toItem: parentView, attribute: .width, multiplier: 1.7, constant: 0)
        
        let aspectRatio = NSLayoutConstraint(item: targetView, attribute: .width, relatedBy: .equal, toItem: parentView, attribute: .height, multiplier: (700/630), constant: 0)
        
        let topSpace = NSLayoutConstraint(item: targetView, attribute: .topMargin, relatedBy: .greaterThanOrEqual, toItem: parentView, attribute: .top, multiplier: 1, constant: 0)
        
        let proportionalHeight = NSLayoutConstraint(item: targetView, attribute: .height, relatedBy: .equal, toItem: parentView, attribute: .height, multiplier: 0.9, constant: 0)
        
        NSLayoutConstraint.activate([centerX, centerY, proportionalWidth, proportionalHeight, aspectRatio, topSpace])
        
    }
    
}
class CandyGradientView1: UIView {
    
    var appSymbol: UIImageView?
    var pinkGradient: CAGradientLayer?
    
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    //    override init(frame: CGRect) {
    //        super.init(frame: frame)
    //        self.loadDesign()
    //    }
    //
    //    required init?(coder aDecoder: NSCoder) {
    //        super.init(coder: aDecoder)
    //        self.loadDesign()
    //    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.colors = [UIColor.clear,MyColors.Black.cgColor,UIColor.black.cgColor]
        //         gradientLayer.colors = [MyColors.pinkForGradient.cgColor, MyColors.blueForGradient.cgColor]
        gradientLayer.locations = [0.0, 0.5, 1.0]
    }
    
    private func loadDesign() {
        self.clipsToBounds = true
        //        self.backgroundColor = .white
        
        DispatchQueue.main.async {
            self.addAppSymbol()
            //            self.addPinkGradient()
        }
    }
    
    private func addAppSymbol() {
        self.appSymbol = UIImageView(frame: .zero)
        self.addSubview(self.appSymbol!)
//        self.addConstraintsToImage(targetView: self.appSymbol!, parentView: self)
        //self.appSymbol?.image = MyImages.appSymbol
        self.appSymbol?.contentMode = .scaleAspectFit
    }
    
    private func addPinkGradient() {
        //        self.pinkGradient = UIView.gradientLayer(locations: [0.0, 0.6, 1.0], colors: [MyColors.blueForGradient, MyColors.pinkForGradient, MyColors.pinkForGradient])
        self.pinkGradient = UIView.gradientLayer(locations: [0.0, 0.5, 1.0], colors: [UIColor.clear, MyColors.Black,UIColor.black])
        self.pinkGradient?.frame = self.bounds
        self.layer.addSublayer(self.pinkGradient!)
        
        if let appSymbol = self.appSymbol {
            self.bringSubviewToFront(appSymbol)
        }
        
    }
    
    
    func addConstraintsToImage(targetView: UIView, parentView: Any) {
        //        let targetView = UIView()
        //        Align centre x to superview
        //        Align centre y to superview
        //        Proportional width to superview 1.7
        //        700:630 aspect ratio
        //        Topspace to sueprview >= 0
        //        Proportional height to superview 0.9
        targetView.translatesAutoresizingMaskIntoConstraints = false
        let centerX = NSLayoutConstraint(item: targetView, attribute: .centerX, relatedBy: .equal, toItem: parentView, attribute: .centerX, multiplier: 1.0, constant: 0)
        
        let centerY = NSLayoutConstraint(item: targetView, attribute: .centerY, relatedBy: .equal, toItem: parentView, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        let proportionalWidth = NSLayoutConstraint(item: targetView, attribute: .width, relatedBy: .equal, toItem: parentView, attribute: .width, multiplier: 1.7, constant: 0)
        
        let aspectRatio = NSLayoutConstraint(item: targetView, attribute: .width, relatedBy: .equal, toItem: parentView, attribute: .height, multiplier: (700/630), constant: 0)
        
        let topSpace = NSLayoutConstraint(item: targetView, attribute: .topMargin, relatedBy: .greaterThanOrEqual, toItem: parentView, attribute: .top, multiplier: 1, constant: 0)
        
        let proportionalHeight = NSLayoutConstraint(item: targetView, attribute: .height, relatedBy: .equal, toItem: parentView, attribute: .height, multiplier: 0.9, constant: 0)
        
        NSLayoutConstraint.activate([centerX, centerY, proportionalWidth, proportionalHeight, aspectRatio, topSpace])
        
    }
    
}
extension UIView {
    
    static func gradientLayer(locations: [NSNumber], colors: [UIColor]) -> CAGradientLayer {
        guard locations.count == colors.count else {
            print("locations count not matched with colors count")
            return CAGradientLayer()
        }
        
        let gradient = CAGradientLayer()
        gradient.locations = locations
        gradient.colors = colors.map{$0.cgColor}
        //        gradient.frame = self.bounds
        
        return gradient
    }
}

extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension Date {
    
    func toString(withFormat format: String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //        dateFormatter.timeZone = TimeZone.current
        let str = dateFormatter.string(from: self)
        return str
    }
}

extension UIViewController {
    
    func presentVC(identifire: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: identifire) as UIViewController
        //        present(vc, animated: true, completion: nil)
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func dismisVC() {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
extension UIColor {
    
    @nonobjc class var MyTealColor: UIColor {
        return UIColor(named: "MyTealColor")!
    }
}
extension UIViewController{
    func PLAYWhenPhone()  {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
        } catch { }

        let theSession = AVAudioSession.sharedInstance()
        NotificationCenter.default.addObserver(self,selector:#selector(UIViewController.playInterrupt),name:AVAudioSession.interruptionNotification,object: theSession)

    }

    @objc func playInterrupt(notification: NSNotification) {

        if notification.name == AVAudioSession.interruptionNotification
            && notification.userInfo != nil {

            let info = notification.userInfo!
            var intValue: UInt = 0
            (info[AVAudioSessionInterruptionTypeKey] as! NSValue).getValue(&intValue)
            if let type = AVAudioSession.InterruptionType(rawValue: intValue) {
                switch type {
                case .began:
                    print("stop song")
                   
//                    callCheck = "YES"
                case .ended:
                    print("Resume song")
                    if  playPauseSelected == 1{
                         
                         appdelegate.audioPlayer.pause()
                         appdelegate.audioPlayer.resume()
                     }
                     if appdelegate.radioPlayer.state == .playing{
                         appdelegate.radioPlayer.pause()
                         appdelegate.radioPlayer.resume()
                     }
                     if isVideoPlay{
                         appdelegate.player.play()
                     }

                @unknown default:
                   print("Defailt")
                }
            }
        }
    }
}
