//
//  LiveVideoModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 13/03/21.
//

import Foundation
public class CommentListModel
{
    public var comment:String = ""
    public var commentDate:String = ""
    public var time:String = ""
    public var commentByName:String = ""
    public var commentImageURL:String = ""
    public var type:String = ""
    public var date: Date? = nil
    
    required public init()
    {
        comment = ""
        commentDate = ""
        commentByName = ""
        commentImageURL = ""
        type = ""
        time = ""
    }
    public class func modelsFromDictionaryArray(array:NSArray) -> [CommentListModel]
    {
        var models:[CommentListModel] = []
        for item in array
        {
            models.append(CommentListModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary)
    {
     
        comment = dictionary["comment"] as? String ?? ""
        time =  dictionary["time"] as? String ?? ""
        commentByName = dictionary["commentByName"] as? String ?? ""
        commentDate = dictionary["commentDate"] as? String ?? ""
        commentImageURL = dictionary["commentImageURL"] as? String ?? ""
        type = dictionary["type"] as? String ?? ""
        
    }
}

struct sponserData {
    var image : String?
    var link : String?
    
    init(dict: [String: Any]) {
        image = getString(dict["image"])
        link = getString(dict["link"])
        
    }
    
}
