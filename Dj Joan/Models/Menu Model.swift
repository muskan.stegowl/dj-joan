//
//  Menu Model.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 10/04/21.
//

import Foundation
struct menuData {
    var menu_type : String?
    var menu_name : String
    var menu_image : String
    init(dict: [String: Any]) {
        menu_name = getString(dict["menu_name"])
        menu_image = getString(dict["menu_image"])
        menu_type = getString(dict["menu_type"])
    }
}
