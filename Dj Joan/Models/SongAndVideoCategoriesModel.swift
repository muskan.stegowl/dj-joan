//
//  SongAndVideoCategoriesModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 12/03/21.
//

import Foundation
struct CategoriesData {
    
    var category_id : Int?
    var menu_id : Int?
    var category_image : String?
    var category_name : String?
    
    init(dict: [String: Any]) {
    
        category_id = getInteger(dict["category_id"])
        menu_id = getInteger(dict["menu_id"])
        category_image = getString(dict["category_image"])
        category_name = getString(dict["category_name"])
        
    }
    
}
