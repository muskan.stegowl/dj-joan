//
//  HomeModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 06/04/21.
//

import Foundation
struct HomeListData {
    
    var category_id : Int?
    var type : String?
    var name : String?
    var image : String?
    var SubCategoryData:[HomeSubcategory] = []
    var SongListData:[songListData] = []
    var VideoListData:[VideoListData] = []
    var VideoCategoryData:[CategoriesData] = []
    init(dict: [String: Any]) {
        
        category_id = getInteger(dict["category_id"])
        type = getString(dict["type"])
        name = getString(dict["name"])
        image = getString(dict["image"])
        
        if type == "TracksCategory"{
            
        if let data = dict["assests"] as? [[String : Any]] {
            for item in data {
                let footer = HomeSubcategory(dict: item)
                self.SubCategoryData.append(footer)
            }
        }
    }else if  type == "Music"{
        if let data = dict["assests"] as? [[String : Any]] {
            for item in data {
                let footer = songListData(dict: item)
                self.SongListData.append(footer)
            }
        }
    }else if type == "Videos"{
        if let data = dict["assests"] as? [[String : Any]] {
            for item in data {
                let footer = Dj_Joan.VideoListData(dict: item)
                self.VideoListData.append(footer)
            }
        }
    }else if type == "VideosCategory"{
        if let data = dict["assests"] as? [[String : Any]] {
            for item in data {
                let footer = CategoriesData(dict: item)
                self.VideoCategoryData.append(footer)
            }
        }
    }
}
}
struct HomeSubcategory {
    var category_for : String?
    var category_id : Int?
    var category_image : String?
    var parent_category_name : String?
    var category_name : String?
    var created_at : String?
    var image_status : Bool?
    var menu_id : Int?
    var type : String?
    init(dict: [String: Any]) {
        category_for = getString(dict["category_for"])
        category_id = getInteger(dict["category_id"])
        category_image = getString(dict["category_image"])
        parent_category_name = getString(dict["parent_category_name"])
        category_name = getString(dict["category_name"])
        created_at = getString(dict["created_at"])
        image_status = getBool(dict["image_status"])
        menu_id = getInteger(dict["menu_id"])
        type =  "TracksCategory"
    }
}

