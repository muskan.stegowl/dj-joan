//
//  LiveRadioModel.swift
//  Maiky Backstage
//
//  Created by Stegowl Macbook Pro on 01/07/21.
//

import Foundation
struct LiveRadioData {
    var song : String?
    var song_id : Int?
    var category_id : Int?
    var categoryitems_id : Int?
    var menu_id : Int?
    var created_at : String?
    var song_image : String?
    var song_name : String?
   
    init(dict: [String: Any]) {
        song_id = getInteger(dict["song_id"])
        menu_id = getInteger(dict["menu_id"])
        categoryitems_id = getInteger(dict["categoryitems_id"])
        category_id = getInteger(dict["category_id"])
        song = getString(dict["song"])
        created_at = getString(dict["created_at"])
        song_image = getString(dict["song_image"])
        song_name = getString(dict["song_name"])
    }
    
}
