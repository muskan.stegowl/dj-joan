//
//  VideoListModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 13/03/21.
//

import Foundation
struct VideoListData {
    
    var categoryitems_id : Int?
    var menu_id : Int?
    var category_id : Int?
    var videos_id : Int?
    var videos_image : String?
    var videos_name : String?
    var videos_description : String?
    var videos_link : String?
    var date : String?
    var time : String?
    var url_type : Int?
    init(dict: [String: Any]) {
        categoryitems_id = getInteger(dict["categoryitems_id"])
        menu_id = getInteger(dict["menu_id"])
        category_id = getInteger(dict["category_id"])
        videos_id = getInteger(dict["videos_id"])
        videos_image = getString(dict["videos_image"])
        videos_name = getString(dict["videos_name"])
        videos_description = getString(dict["videos_description"])
        videos_link = getString(dict["videos_link"])
        date = getString(dict["date"])
        time = getString(dict["time"])
        url_type = getInteger(dict["url_type"])
    }
    
}
