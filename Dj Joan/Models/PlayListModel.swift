//
//  PlayListModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 15/03/21.
//

import Foundation
struct PlayListData {
    
    var playlist_id : Int?
    var user_id : Int?
    var playlist_name : String?
   
    init(dict: [String: Any]) {
        
        playlist_id = getInteger(dict["playlist_id"])
        user_id = getInteger(dict["user_id"])
        playlist_name = getString(dict["playlist_name"])
    }
}
