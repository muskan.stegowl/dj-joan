//
//  SongListModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 13/03/21.
//

import Foundation
struct songListData {
    
    var categoryitems_id : Int?
    var likes_count : Int?
    var favourites_count : Int?
    var playlistsongs_id : Int?
    var playlist_id : Int?
    var total_played : Int?
    var menu_id : Int?
    var song_id : Int?
    var total_shared : Int?
    
    var likes_status : Bool?
    var favourites_status : Bool?
    
    var song : String?
    var song_image : String?
    var song_artist : String?
    var song_duration : String?
    var song_name : String?
    
    init(dict: [String: Any]) {
        categoryitems_id = getInteger(dict["categoryitems_id"])
        likes_count = getInteger(dict["likes_count"])
        favourites_count = getInteger(dict["favourites_count"])
        playlistsongs_id = getInteger(dict["playlistsongs_id"])
        playlist_id = getInteger(dict["playlist_id"])
        total_played = getInteger(dict["total_played"])
        menu_id = getInteger(dict["menu_id"])
        song_id = getInteger(dict["song_id"])
        total_shared = getInteger(dict["total_shared"])
        
        likes_status = getBool(dict["likes_status"])
        favourites_status = getBool(dict["favourites_status"])
        
        song = getString(dict["song"])
        song_image = getString(dict["song_image"])
        song_artist = getString(dict["song_artist"])
        song_name = getString(dict["song_name"])
        song_duration = getString(dict["song_duration"])
        
    }
    
}
