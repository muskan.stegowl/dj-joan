//
//  NotificationModel.swift
//  Dj Kenny
//
//  Created by Stegowl Macbook Pro on 15/03/21.
//

import Foundation
struct notiData {
  
    var title : String?
    var message : String?
    var date : String?

    init(dict: [String: Any]) {
       
        title = getString(dict["title"])
        message = getString(dict["message"])
        date = getString(dict["created_at"])
    }
    
}
